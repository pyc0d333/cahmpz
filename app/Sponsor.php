<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    protected $fillable = ['position'];


    public function users()
    {
        return $this->hasMany('App\Users');
    }
}
