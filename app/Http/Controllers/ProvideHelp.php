<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\User;
use App\Package;
use App\Receiver;
use App\Sponsor;

class ProvideHelp extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    protected $package;
    protected $receiver;
    protected $sponsor;

    public function __construct(Request $request,Sponsor $sponsor, Receiver $receiver, User $user, Package $package)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->receiver = $receiver;
        $this->package = $package;

    }


    /**
     * User signs up to a package
     * @params :userId, Package,
     * @return a random user
     */

    public function provideHelp($id, Request $request)
    {

        $asking_time = Auth::user()->created_at;
        dd($asking_time);
        $package = $request->input('package');
        $package = $this->package->where('amount', $package)->first();
        $packageId = $package->id;
        $user = $this->user->find($id);
        $sponsor = $this->user->where('awaiting_help', true)
            ->where('package_id', $packageId )
            ->first();


        if($sponsor == null)
        {
            $sponsor = $this->user->where('package_id', $packageId)->first();
            $user->package_id = $package->id;
            $user->sponsor = $sponsor->id;
            $user->giving_help = true;
            $user->save();
            return redirect()->route('sponsor')
                ->with('sponsors', $sponsor);
        }

        $user->sponsor = $sponsor->id;
        $user->package_id = $package->id;
        $user->giving_help = true;
        $user->save();
        return redirect()->route('sponsor')
            ->with('sponsor', $sponsor);
    }


    public function addToLeft($id)
    {
        $sponsor = $this->user->find($id);
    }

    public function getHelp($id, Request $request)
    {
        $user = $this->user->find($id);
        $user->awaiting_help = true;
        flash('You have successfully requested for help, wait to get matched');
        return redirect()->route("dashboard-home")->with('message', 'Success');

    }


}
