<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Pin;
use App\User;
use Mail;
use App\Package;

class UserController extends Controller
{

    protected $user;
    protected  $package;

    public function __construct(User $user, Pin $pins, Package $package)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->package = $package;
        $this->pins = $pins;
    }


    public  function add_pin()
    {
        if(Auth::user()->has_pin || Auth::user()->is_admin == true ||  Auth::user()->is_superadmin == true)
        {
            return redirect()->route('profile');
        }

        else
        {

            return view('auth.token');

        }
    }


    public function validatePin(Request $request)
    {

        $this->validate($request,['pin' => 'required|numeric|digits:6']);

        $token = $request->input('pin');
        $all_pins  = \App\Pin::pluck('pin');
        $found =  false;


        do {

            foreach ($all_pins as $pin) {

                if ($token == $pin) {
                    $user = Auth::user()->id;
                    $user = $this->user->find($user);
                    $user->pin = $token;
                    $user->is_activated = true;
                    $user->has_pin = true;
                     $user->save();
                    $found = true;

                    flash('Your Pin was valid, Welcome to Champdreams', 'success');
                    return view('home');

                }

            }

            if ($found == false)
            {
                flash('Sorry, this token is invalid', 'warning');
                return redirect()->route('token_page');

            }
        }
            while($found == false);




    }

    public function paymentInfo($id, Request $request)

    {

        $this->validate($request, [
            'name' => 'required',
            'bank' => 'required',
            'acct' => 'required|digits:10',
//            'phone' => 'required|digits:11',
        ]);


        $name = $request->input('name');
        $bank = $request->input('bank');
        $acct = $request->input('acct');
//        $phone = $request->input('phone');
        $me = $this->user->find($id);

        $me->acct_name = $name;
        $me->bank_name = $bank;
        $me->account_number = $acct;
//            $me->phone = $phone;
        $me->save();
        flash('You have successfully updated your bank details.  Click on Home to perform another transaction', 'success');

        if($me->is_admin == false)
        {
            $me->acct_name = $name;
            $me->bank_name = $bank;
            $me->account_number = $acct;
//            $me->phone = $phone;
            $me->save();
            flash('You have successfully updated your bank details.  Click on Home to perform another transaction', 'success');
        }

        return redirect()->back();

    }


    public function add_user(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:50',
            'acct_name' => 'required|max:50',
            'bank' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'acct' => 'required|digits:10',
            'phone' => 'required|digits:11',
            'package' => 'required',
        ]);

        $user  = new User();
        $user->name = $request->input('name');
        $user->package_id = $request->input('package');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->phone = $request->input('phone');
        $user->account_number = $request->input('acct');
        $user->acct_name = $request->input('acct_name');
        $user->bank_name = $request->input('bank');

        $user->save();

        flash('New User Added', 'success');

        return redirect()->back();

    }

    public function update_user(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|max:50',
            'acct_name' => 'required|max:50',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|confirmed',
            'acct' => 'required|digits:10',
            'phone' => 'required|digits:11',
            'package' => 'required',
            'bank_name' => 'required',
        ]);

        $amount = $request->input('package');

        $package = $this->package->where('amount', $amount)->first();

        $user = $this->user->find($id);
        $user->account_number = $request->input('acct');
        $user->package_id = $package->id;
        $user->password = $request->input('password');
        $user->account_number = $request->input('acct');
        $user->bank_name = $request->input('bank_name');
        $user->email = $request->input('email');
        $user->save();
        flash('user successfully updated', 'success');

        return redirect()->route('user_details_view',$user->id);
    }


    public function delete_user($id)
    {
        $user = $this->user->find($id);
        $user->delete();
        flash('User deleted Successfully', 'success');
        return redirect()->route('list-users');
    }

}
