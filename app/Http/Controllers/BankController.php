<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.admin.add_bank');
    }


    public function save(Request $request)
    {

          $this->validate($request,[
            'name' =>'required|string'
        ]);


        $bank = new Bank();
        $name = $request->input('name');
        $bank->name = $name;
        $bank->save();
        flash('Successfully Added Bank', 'success');
        return redirect()->route('bank');

    }



    public function delete($id)
    {
        $bank = Bank::find($id);
        $bank->delete();
        flash('Data Deleted', 'success');
        return redirect()->back();

    }



    public function edit($id)
    {

    }
}

