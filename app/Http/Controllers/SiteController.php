<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Package;
use App\Order;
use Carbon\Carbon;

class SiteController extends Controller
{
    public function __construct(User $user, Order $order, Package $package)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->package = $package;
        $this->order = $order;


    }



    public function home()
    {

        $user = Auth::user();
        $packages = $this->package->all();

        if(Auth::user()->has_pin == false)
        {
            flash('You are here because you have not submitted any token', 'danger');
            return redirect()->route('token_page');
        }

        if($user->left != 0 || $user->right != 0)
        {
            flash('You have a donor(s) to be confirmed', 'info');
            return view('dashboard.matches');
        }


        if($user->is_awaiting_sponsor == 1)
        {
            flash('You still need to be patient to get paired with a sponsor');
            return view('dashboard.awaiting_sponsor');
        }



        if($user->help_count == 1 && $user->completed_initial_receipt = 0   && $user->received_count == 0  && $user->awaiting_payment == false && $user->is_awaiting_sponsor == false)
        {
            return view('dashboard.initial_get_help')->with('packages', $packages);

        }

        if($user->completed_initial_help  &&  $user->sponsor == 0 && $user->is_awaiting_help == false && $user->receiver)
        {

            return view('dashboard.get_help_two')->with('packages', $packages);
        }

        if($user->is_superadmin)
        {
            return redirect()->route('get_help_requests');
        }

        if($user->awaiting_help)
        {
            return redirect()->route('matches');
        }

        if($user->is_awaiting_sponsor == true)
        {
            flash('You still need to be patient to get paired with a sponsor');
            return view('dashboard.awaiting_sponsor');
        }


        if($user->help_count == 1 && $user->receiver  && $user->sponsor == 0)
        {
            flash('Please Submit a get help request to get your first help on Champdreams ', 'success');
            return view('dashboard.initial_get_help')->with('packages', $packages);

        }
        elseif($user->account_number == null || $user->phone == null || $user->bank_name == ''  )
        {
            flash('You need to add your account details before you can use this website', 'warning');
            return redirect()->route('info');
        }


        elseif ($user->left != 0)
        {
            flash('you already have a donor');
            return redirect()->route('matches');
        }
        elseif($user->left != 0 || $user->right != 0 )
        {
            flash('You still have pending transactions to approve', 'warning');
            return redirect()->route('matches');
        }

        elseif($user->can_recycle && $user->receiver == false && $user->is_awaiting_sponsor == 0 && $user->sponsor== 0 && $user->given_help == 0)
        {
            return redirect()->route('provide_help_two');
        }

        elseif($user->can_recycle  && $user->receiver && $user->is_awaiting_sponsor == false && $user->sponsor == 0 && $user->given_help == 1 && $user->awaiting_donor == false)
        {
//            flash('Get Help To Be Merged with 2 people', 'success');

            return view('dashboard.get_help_two');
        }

        elseif($user->is_awaiting_sponsor == 1)
        {
            flash('You still need to be patient to get paired with a sponsor');
            return view('dashboard.awaiting_sponsor');
        }

        elseif($user->help_count == 1 && $user->received_count == 0 &&  $user->awaiting_donor == false )
        {
            flash('Please Submit a get help request to get your first help on Champdreams ', 'success');
            return view('dashboard.initial_get_help')->with('packages', $packages);

        }


        elseif (Auth::user()->awaiting_payment_confirmation)
        {
            return view('dashboard.awaiting_payment_confirmation');
        }


        elseif (Auth::user()->giving_help && Auth::user()->sponsor != 0)
        {
            return view('dashboard.sponsor');
        }

        elseif($user->awaiting_help && $user->left != 0)
        {
            flash('You still have a pending payment to be approved');
            return views('dashboard.matches');
        }


        elseif($user->left != 0 || $user->right != 0)
        {
            flash('You have pending payment(s)');
            return view('dashboard.matches');
        }

        elseif ($user->sponsor != 0)
        {
            return view('dashboard.sponsor');
        }
        elseif($user->help_count == 1 && $user->received_count == 1 && $user->completed_initial_receipt == true && $user->is_awaiting_sponsor == 0 && $user->completed_initial_help == true)
        {
            flash('You Need to Provide another Help to Unlock your remaining balance');
            return redirect()->route('provide_help_two');
        }

        elseif($user->completed_initial_help  &&  $user->sponsor == 0)
        {

            return view('dashboard.initial_give_help')->with('packages', $packages);
        }

//
        elseif(Auth::user()->completed_initial_help  && Auth::user()->sponsor == 0 && Auth::user()->help_count == 1 && $user->is_awaiting_sponsor != true )
        {

            return view('dashboard.initial_get_help')->with('packages', $packages);
        }

        elseif (Auth::user()->awaiting_donor && $user->help_count > 1 ){
            flash('AWAITING DONOR. PLEASE BE PATIENT AS WE LOOK TO PAIR YOU', 'success');
            return view('dashboard.home');
        }



       elseif (Auth::user()->is_recycling)
        {
            return view('dashboard.home');
        }

        else
        {
            return view('dashboard.home')->with([
                'packages'=> $packages
            ]);

        }
    }


    public function profile()
    {


//        if(Auth::user()->has_pin == false)
//        {
//            flash('You are here because you have not submitted any token', 'danger');
//            return redirect()->route('token_page');
//        }

        $user  = $this->user->find(Auth::user()->id);
        $sponsorId = $user->sponsor;
        $sponsor  = $this->user->find($sponsorId);
        return view('dashboard.profile')->with('sponsor', $sponsor);

    }


    public function info()
    {


        if(Auth::user()->has_pin == false)
        {
            flash('You are here because you have not submitted any token', 'danger');
            return redirect()->route('token_page');
        }
        return view('dashboard.info');

    }

    public function sponsor()
    {
        $user = Auth::user();

        if($user->account_number == null || $user->phone == null || $user->bank_name == ''  )
        {
            flash('You need to add your account details before you can use this website', 'warning');
            return redirect()->route('info');
        }

        return view('dashboard.sponsor');

    }

    public function earnings()
    {

    }

    public function add_user()
    {
        return view('dashboard.admin.add_user');

    }

    public function awaiting_payment_confirmation()
    {
        if(!Auth::user()->awaiting_payment_confirmation)
        {
            flash('your payment has been confirmed', 'success');
            return view('dashboard.home');
        }
        return view('dashboard.awaiting_payment_confirmation');

    }

    public function awaiting_donor()
    {
        if(Auth::user()->awaiting_help)
        {
            flash('A donor has already been assigned to you. Wait for their call/message to confirm your account');
            return view('dashboard.home');
        }

        if(Auth::user()->is_recyling)
        {
            flash('You need to recycle your account to unlock the other donor payment');
            return view('dashboard.home');
        }
        return view('dashboard.awaiting_help');

    }


    public function list_users()
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        $users = $this->user->all();
        return view('dashboard.admin.list_users')->with('users', $users);
    }


    public function update_user_view($id)
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(404);
        }
        $user = $this->user->find($id);
        return view('dashboard.admin.edit_user')->with('user', $user);
    }


    public function provide_help_two()
    {
        return view('dashboard.provide_help_two');
    }


    public function user_details_view($id)
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(404);
        }
        $user = $this->user->find($id);
        return view('dashboard.admin.user_details')->with('user', $user);
    }


    //helpers matched to sponsor legs
    public function matches()
    {
        $packages = $this->package->all();

        $user = $this->user->find(Auth::user()->id);
        $left = $this->user->find($user->left);
        $right = $this->user->find($user->right);

        $sponsor = $this->user->find($user->sponsor);

        if($user->account_number == null || $user->phone == null || $user->bank_name == ''  )
        {
            flash('You need to add your account details before you can use this website', 'warning');
            return view('dashboard.info');
        }

        if($user->left != 0 || $user->right != 0)
        {
            flash('You have a donor(s) to be confirmed', 'info');
            return view('dashboard.matches');
        }


        if($user->help_count == 1 && $user->receiver  && $user->sponsor == 0 )
        {
            flash('Please Submit a get help request to get your first help on Champdreams ', 'success');
            return view('dashboard.initial_get_help')->with('packages', $packages);

        }

        if($user->help_count == 1 && $user->completed_initial_help == 0 && $user->completed_initial_receipt == 0   && $user->received_count == 0  && $user->awaiting_payment == false && $user->is_awaiting_sponsor == false)
        {
            flash('Please Submit a get help request to get your first help on Champdreams ', 'success');

            return view('dashboard.initial_get_help')->with('packages', $packages);

        }

        if($user->help_count == 1 && $user->completed_initial_receipt == 0   && $user->received_count == 0  && $user->awaiting_payment == false && $user->is_awaiting_sponsor == false)
        {
            flash('Please Submit a get help request to get your first help on Champdreams ', 'success');
            return view('dashboard.initial_get_help')->with('packages', $packages);

        }

        if($user->is_awaiting_sponsor == 1)
        {
            flash('You still need to be patient to get paired with a sponsor');
            return view('dashboard.awaiting_sponsor');
        }


        if ($user->left != 0)
        {
            flash('you already have a donor');
            return view('dashboard.matches');
        }

        if($user->is_awaiting_sponsor == 1)
        {
            flash('You still need to be patient to get paired with a sponsor');
            return view('dashboard.awaiting_sponsor');
        }

        if($user->sponsor != 0)
        {
            return view('dashboard.matches');
        }

        if(!Auth::user()->completed_initial_help  && Auth::user()->sponsor == 0 && $user->is_awaiting_sponsor == false)
        {

            return view('dashboard.initial_give_help')->with('packages', $packages);
        }


//        elseif($user->can_recycle  && $user->receiver && $user->is_awaiting_sponsor == false && $user->sponsor == 0 && $user->given_help == 1)
//        {
//            flash('Get Help To Be Merged with 2 people', 'success');
//
//            return view('dashboard.get_help_two');
//        }

        if($user->can_recycle && $user->is_awaiting_sponsor == 0  && $user->completed_initial_receipt && $user->given_help == 0 && $user->sponsor == 0)
        {
            return redirect()->route('provide_help_two');
        }

       if($user->help_count == 1 && $user->awaiting_payment == false && $user->is_awaiting_sponsor == false)
        {
            return view('dashboard.initial_get_help')->with('packages', $packages);

        }

       elseif($user->left != 0 || $user->right != 0 )
       {
           flash('You still have pending transactions to approve', 'warning');
           view('dashboard.matches');
       }
       elseif ($user->left != 0)
       {
            return view('dashboard.matches')->with([
               'left' => $left,
               'right' => $right,
               'user' => $user,
               'sponsor' => $sponsor
           ]);

       }

       elseif($user->can_recycle && $user->is_awaiting_sponsor == 0 && $user->sponsor== 0 && $user->given_help == 0)
       {
           return redirect()->route('provide_help_two');
       }


       elseif($user->help_count >= 1 && $user->received_count == 1 && $user->completed_initial_receipt == true && $user->is_awaiting_sponsor == 0 && $user->completed_initial_help == true)
       {
           flash('You Need to Provide another Help to Unlock your remaining balance');
           return redirect()->route('provide_help_two');
       }

//        if($left == null && $right == null)
//        {
//            flash('you have not been matched with anyone now', 'info');
//            return view('dashboard.profile');
//        }



//        else{
//            $left_order = $this->order->where('user_id', $left->id)
//                ->where('type', 'provide_help')->where('is_completed', true)
//                ->orderBy('updated_at', 'desc')->first();
//
//
////            $left_order_time = $left_order->updated_at->diffForHumans();
//            return view('dashboard.matches')->with([
//                'left' => $left,
//                'right' => $right,
//                'user' => $user
////                'order_time' => $left_order_time
//            ]);
//
//        }

            return view('dashboard.matches')->with([
                'left' => $left,
                'right' => $right,
                'user' => $user,
                'sponsor' => $sponsor
            ]);




    }

}
