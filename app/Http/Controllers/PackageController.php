<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.admin.packages');
    }


    public function save(Request $request)
    {
        $this->validate($request,[
            'amount' =>'required|string'
        ]);

        $package = new Package();
        $amount = $request->input('amount');
        $package->amount = $amount;
        $package->save();
        flash('Package Added Successfully', 'success');
        return redirect()->route('packages');

    }



}
