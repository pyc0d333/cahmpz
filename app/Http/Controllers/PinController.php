<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Pin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PinController extends Controller
{

    protected $user;
    protected $pin;

    public function __construct(User $user, Pin $pin)
    {
        $this->user = $user;
        $this->pin = $pin;
    }


    public function index()
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        return view('dashboard.admin.pins');
    }

    public function save($id, $count)
    {

//        DB::table('pins')->delete();


        if(Auth::user()->is_superadmin == false)
        {
            abort(404);
        }
        $counter = 1;

        do {
            \App\Pin::updateOrCreate(
                ['owner_id' => Auth::user()->id, 'pin' => mt_rand(000000,999999)]
            );
            $counter += 1;
        }
        while($counter < $count);

        $pins = $this->pin->where('owner_id', $id)->get();

        $new_pins = $this->pin->where('owner_id', $id)->get();

        flash('New Pins Successfully Generated', 'success');
        return redirect()->route('pins')->with([
            'pin'=>$pins,
            'recent_pins'=>$pins
          ]);
    }

}
