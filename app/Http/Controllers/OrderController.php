<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\User;
use App\Package;
use Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    protected $user;
    protected $package;

    public function __construct(User $user, Request $request, Package $package, Order $order)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->order = $order;
        $this->package = $package;
    }


    public function save(Request $request, $id)
    {

        $order = new Order();
        $user = $this->user->find($id);
        $package = $request->input('package');
        $package = $this->package->where('amount', $package)->first();
        $packageId = $package->id;

        $order->type = 'provide_help';
        $order->user_id = $user->id;
        $order->package_id = $packageId;
        $order->save();

        $user->package_id = $packageId;
        $user->is_awaiting_sponsor = true;
        $user->given_help  = false;
        $user->giving_help  = false;

        $user->save();

        if($user->completed_initial_help == 0)
        {
            flash('You have requested to provide your first help on champdreams. Congratutions', 'success');
            flash('Please be patient as we get someone to merge you with', 'info');
            return view('partials.complete_help_success');
        }



        flash('Request Received', 'success');

        return redirect()->back();
    }


    public function recycle(Request $request, $id)
    {

        $order = new Order();
        $user = $this->user->find($id);
        $package = $user->package_id;

        $order->type = 'provide_help';
        $order->user_id = $user->id;
        $order->package_id = $package;
        $order->save();

        $user->package_id = $package;
        $user->is_awaiting_sponsor = true;
        $user->awaiting_donor = false;
        $user->awaiting_help  = false;
        $user->giving_help  = false;
        $user->given_help  = false;
        $user->is_recycling = true;
        $user->save();
        flash('Your Request has been received. Please wait patiently to be paired with a sponsor', 'success');
        return view('dashboard.awaiting_sponsor');

    }


    public function provide_help_requests()
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        return view('dashboard.admin.provide_help');
    }

    public function get_help_requests()
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        return view('dashboard.admin.awaiting_help_requests');
    }

    public function view_provide_help_order($id)
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        $order =  $this->order->find($id);
        $order_time =  $order->created_at->diffForHumans();
        $package =  $this->package->find($order->package_id);
        $user =  $this->user->find($order->user_id);

        $users =  $this->user
            ->where('awaiting_donor', true)
            ->where('package_id', $package->id)
            ->where('id', '!=', $user->id)
            ->get();

        return view('dashboard.admin.add_sponsor')->with([
            'order' =>$order ,
            'user'=>$user,
            'users'=>$users,
            'package'=> $package,
            'order_time'=> $order_time
        ]);
    }

    public function view_get_help_order($id)
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        $order =  $this->order->find($id);
        $order_time =  $order->created_at->diffForHumans();
        $package =  $this->package->find($order->package_id);
        $user =  $this->user->find($order->user_id);


        $users =  $this->user
            ->where('is_awaiting_sponsor', true)
            ->where('package_id', $package->id)
            ->where('id', '!=', $user->id)
            ->get();

        return view('dashboard.admin.add_donor')->with([
            'order' =>$order ,
            'user'=>$user,
            'users'=>$users,
            'package'=> $package,
            'order_time'=> $order_time
        ]);
    }


    //provide sponsor for a get help Request
    public function provide_sponsor(Request  $request , $id)
    {


        $this->validate($request, ['sponsor'=>'required|numeric']);
        $sponsorId = $request->input('sponsor');
        $sponsor  = $this->user->find($sponsorId);

        $order =  $this->order->find($id);
        $order->is_completed = true;
        $order->save();

        $package =  $this->package->find($order->package_id);


        $user =  $this->user->find($order->user_id);

        //get the get help order of the donor and mark it as completed

        if(!$sponsor->is_admin)
        {
            $sponsororder = $this->order->where('user_id', $sponsor->id)->where('type', 'awaiting_help')->orderBy('created_at', 'desc')->first();
            $sponsororder->is_completed = true;
            $sponsororder->save();
        }


        $sponsor->left = $user->id;
        $sponsor->awaiting_help = true;
        $sponsor->awaiting_donor = false;
        $sponsor->save();

        //add sponsor to user
        $user->sponsor = $sponsorId;

        $user->giving_help = true;
        $user->is_awaiting_sponsor = false;
        $user->save();

        flash('Successfully assigned sponsor to user', 'success');
        return redirect()->route('provide_help_requests');

    }



    // donor is the user who was assigned to pay this user
    // user is the person who asked for help
    public function provide_donor(Request  $request , $id)
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }

        $this->validate($request, ['donor'=>'required|numeric']);
        $donorId = $request->input('donor');
        $donor  = $this->user->find($donorId);

        //get the provide help order of the donor and mark it as completed
        $donororder = $this->order->where('user_id', $donorId)->where('type', 'provide_help')->orderBy('created_at', 'desc')->first();
        $donororder->is_completed = true;
        $donororder->save();


        //mark this get help order as completed
        $order =  $this->order->find($id);
        $order->is_completed = true;
        $order->save();


        //the user  who asked for help
        $user =  $this->user->find($order->user_id);
        $user->left = $donorId;
        $user->awaiting_help = true;
        $user->awaiting_donor = false;
        $user->save();


        //reset the donor to give help
        $donor->is_awaiting_sponsor = false;
        $donor->sponsor = $user->id;
        $donor->giving_help = true;
        $donor->save();
        $package =  $this->package->find($order->package_id);
        flash('Successfully assigned donor to user', 'success');
        return redirect()->route('get_help_requests');

    }


    //donor is the user who was assigned to pay this user
    // user is the person who asked for help
    public function provide_2_donors(Request  $request , $id)
    {
        if(Auth::user()->is_superadmin == false)
        {
            abort(403);
        }

        $this->validate($request, ['donor1'=>'required|numeric','donor2'=>'required|numeric']);
        $donor1Id = $request->input('donor1');
        $donor2Id = $request->input('donor2');


        if ($donor2Id == $donor1Id)
        {
            flash('you cannot place the same donor twice', 'danger');
            return redirect()->back();
        }


        $donor1  = $this->user->find($donor1Id);
        $donor2 = $this->user->find($donor2Id);


        //get help order true
        $order =  $this->order->find($id);
        $order->is_completed = true;
        $order->save();


        $user =  $this->user->find($order->user_id);
        $user->left = $donor1Id;
        $user->right = $donor2Id;
        $user->awaiting_help = true;
        $user->awaiting_donor = false;
        $user->save();

        $donor1->is_awaiting_sponsor = false;
        $donor1->sponsor = $user->id;
        $donor1->giving_help = true;
        $donor1->save();

        $donor2->is_awaiting_sponsor = false;
        $donor2->sponsor = $user->id;
        $donor2->giving_help = true;
        $donor2->save();


        //get the provide help order of the donor and mark it as completed
        $donor1order = $this->order->where('user_id', $donor1->id)->where('type', 'provide_help')->orderBy('created_at', 'desc')->first();
        $donor1order->is_completed = true;
        $donor1order->save();

        //get the provide help order of the donor and mark it as completed
        $donor2order = $this->order->where('user_id', $donor2->id)->where('type', 'provide_help')->orderBy('created_at', 'desc')->first();
        $donor2order->is_completed = true;
        $donor2order->save();

        $package =  $this->package->find($order->package_id);
        flash('Successfully assigned donors to user', 'success');
        return redirect()->route('get_help_requests');

    }




    //the user providing help confirms that he/she has paid.
    public function provider_payment_confirmation_request($userId)
    {

        $user = $this->user->find($userId);
        $user->confirm_me = true;
        $user->awaiting_payment_confirmation = true;
        $user->save();
        flash('You have successfully requested for payment confirmation', 'success');
        return redirect()->route('awaiting_payment_confirmation');

    }


    //confirm that helper has paid into your account
    public function confirm_provider_payment($id, $sponsorId)
    {
        $provider = $this->user->find($id);
        $provider ->given_help = true;
        $provider ->giving_help = false;
        $provider ->help_count += 1;
        $provider ->confirm_me = false;
        $provider ->helper = false;
        $provider ->receiver = true;
        $provider ->completed_initial_help = true;
        $provider ->is_awaiting_sponsor = false;
        $provider ->awaiting_payment_confirmation = false;
        $provider ->sponsor = 0;

        if($provider->is_recycling)
        {
            $provider->is_recycling = true;
            $provider ->giving_help = false;
            $provider ->given_help = true;
            $provider ->confirm_me = false;
            $provider ->helper = false;
            $provider ->completed_initial_help = true;
            $provider ->receiver = true;
            $provider ->help_count += 1;
            $provider ->is_awaiting_sponsor = false;
            $provider ->awaiting_payment_confirmation = false;
            $provider ->completed_cycle = true;
            $provider->can_recycle = true;

            $provider ->save();


            $sponsor  = $this->user->find($sponsorId);
            $sponsor->left = 0;
            $sponsor->received_count += 1;
            $sponsor->awaiting_help = false;
            $sponsor->completed_initial_receipt = true;
            $sponsor->can_recycle = true;

            $sponsor->receiver= false;
            $sponsor->helper = true;
            $sponsor->given_help = false;
            $sponsor->save();
            flash('You have confirmed that this user has  paid you. Thank you', 'success');
            return redirect()->route('dashboard-home');
        }

        else{
            $provider->completed_cycle = true;
            $provider ->save();
            $sponsor  = $this->user->find($sponsorId);
            $sponsor->left = 0;
            $sponsor->given_help = false;
            $sponsor->awaiting_help = false;
            $sponsor->is_awaiting_sponsor = false;
            $sponsor->helper = 1;
            $sponsor->receiver = 0;
            $sponsor->received_count  += 1;
            $sponsor->can_recycle = true;
            $sponsor->completed_initial_receipt = true;
            $sponsor->is_recycling = true;
            $sponsor->save();

            flash('You have confirmed that this user has  paid you. Thank you', 'success');
            return redirect()->route('dashboard-home');

        }



    }


  //confirm that helper has paid into your account
    public function confirm_provider_payment_r($id, $sponsorId)
    {

        $provider = $this->user->find($id);
        $provider ->given_help = true;
        $provider ->giving_help = false;
        $provider ->help_count += 1;
        $provider ->confirm_me = false;
        $provider ->helper = false;
        $provider ->receiver = true;
        $provider ->completed_initial_help = true;
        $provider ->is_awaiting_sponsor = false;
        $provider ->awaiting_payment_confirmation = false;
        $provider ->sponsor = 0;

        if($provider->is_recycling)
        {
            $provider->is_recycling = true;
            $provider ->giving_help = false;
            $provider ->given_help = true;
            $provider ->confirm_me = false;
            $provider ->helper = false;
            $provider ->completed_initial_help = true;
            $provider ->receiver = true;
            $provider ->help_count += 1;
            $provider ->is_awaiting_sponsor = false;
            $provider ->awaiting_payment_confirmation = false;
            $provider ->completed_cycle = true;
            $provider->can_recycle = true;
            $provider ->save();




            $sponsor  = $this->user->find($sponsorId);
            $sponsor->right = 0;
            $sponsor->received_count += 1;
            $sponsor->awaiting_help = false;
            $sponsor->is_awaiting_sponsor = false;
            $sponsor->completed_initial_receipt = true;
            $sponsor->can_recycle = true;
            $sponsor->receiver= false;
            $sponsor->helper = true;
            $sponsor->given_help = false;
            $sponsor->save();
            flash('You have confirmed that this user has  paid you. Thank you', 'success');
            return redirect()->route('dashboard-home');

        }

        else{
            $provider->completed_cycle = true;
            $provider ->save();
            $sponsor  = $this->user->find($sponsorId);
            $sponsor->right = 0;
            $sponsor->given_help = false;
            $sponsor->awaiting_help = false;
            $sponsor->received_count  += 1;
            $sponsor->helper = 1;
            $sponsor->receiver = 0;
            $sponsor->can_recycle = true;
            $sponsor->completed_initial_receipt = true;
            $sponsor->is_recycling = true;
            $sponsor->save();

            flash('You have confirmed that this user has  paid you. Thank you', 'success');
            return redirect()->route('dashboard-home');

        }
    }


    public function getHelp($id)
    {

        $order = new Order();
        $user = $this->user->find($id);
        $package = $user->package_id;
        $package = $this->package->find($package);
        $package = $this->package->where('amount', $package->amount)->first();
        $packageId = $package->id;

        $order->type = 'awaiting_help';
        $order->user_id = $user->id;
        $order->package_id = $packageId;
        $order->save();

        $user->package_id = $packageId;
        $user->awaiting_donor = true;
        $user->save();

        if($user->completed_initial_receipt == 0)
        {
            flash('You have requested to receive  help on champdreams. Congratutions', 'success');
            flash('Please be patient as we get someone to merge you with', 'info');
            return view('partials.get_help_success');
        }

        else
        {
            flash('You have requested to receive help on champdreams. Wait to be paired in 0 - 5 days', 'success');
            flash('Please be patient as we get someone to merge you with', 'info');
            return view('partials.get_help_success');
        }

        return redirect()->route('awaiting_donor');
    }

}
