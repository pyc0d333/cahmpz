<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Auth;

class EmailController extends Controller
{
    //

    public function send ()
    {

        Mail::send('emails.send', ['user'=>Auth::user()], function ($message){
               $message->to('barjebernard@gmail.com');
        });
        return response()->json(['message' => 'Request completed']);

    }
}
