<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Ticket;
use Illuminate\Http\Request;


class TicketController extends Controller
{
    public function __construct(User $user, Request $request, Ticket $tickect )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->ticket = $tickect;

    }


    public function index()
    {
        if(Auth::user()->is_admin == false || Auth::user()->is_superadmin == false)

            return view('dashboard.ticket');
    }


    public function admin_index()
    {
        if(Auth::user()->is_admin == false || Auth::user()->is_superadmin == false)

            $resolved = $this->ticket->where('resolved', true)->get();
        $unresolved = $this->ticket->where('resolved', false)->get();
        $all = $this->ticket->all();


        return view('dashboard.admin.tickets')->with(
            [
                'all'=>$all,
                'unresolved'=>$unresolved,
                'resolved'=>$resolved
            ]
        );
    }



    public function details($id)
    {
        if(Auth::user()->is_admin == false || Auth::user()->is_superadmin == false)

            $ticket = $this->ticket->find($id);
        $user = $this->user->find($ticket->user_id);
        return view('dashboard.admin.ticket_details')->with(
            [
                'ticket'=>$ticket,
                'user'=>$user

            ]
        );
    }

    public function resolve_ticket($ticketId)
    {
        if(Auth::user()->is_admin == false || Auth::user()->is_superadmin == false)
        {
            abort(403);
        }
        $ticket = $this->ticket->find($ticketId);
        $ticket->resolved = true;
        $ticket->save();
        flash('Ticket resolved', 'success');
        return redirect()->route('list_tickets');
    }


    public function save(Request $request, $id)
    {

        $this->validate($request, [
            'message' => 'required|min:30',
        ]);

        $user = $this->user->find($id);

        $ticket = new Ticket();
        $ticket->message = $request->input('message');
        $ticket->user_id = $id;
        $ticket->ticket_no = mt_rand(103, 500500);
        $ticket->save();
        flash('Your message has been received, we will get back to you shortly', 'success');
        $tickets = $this->ticket->where('user_id', Auth::user()->id)->get();

        return  redirect()->back();
    }
}