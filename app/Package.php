<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Package extends Model
{
    protected  $fillable = ['name', 'amount', 'description'];

    public function users()
    {
        return $this->hasMany(App\User);
    }
}
