<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsertbale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone');

            $table->string('address')->nullable();
            $table->integer('account_number')->nullable();
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_superadmin')->default(false);
            $table->boolean('is_activated')->default(false);
            $table->boolean('awaiting_help')->default(false);
            $table->boolean('giving_help')->default(false);
            $table->boolean('given_help')->default(false);
            $table->boolean('has_pin')->default(false);
            $table->integer('sponsor')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('acct_name')->nullable();
            $table->integer('left')->default(0);
            $table->integer('right')->default(0);
            $table->integer('pin')->default(0);
            $table->decimal('total_earnings')->default(0);
            $table->decimal('total_payments')->default(0);;
            $table->string('referrer_id')->default(0);
            $table->integer('package_id')->nullable();
            $table->boolean('completed_initial_help')->default(false);
            $table->boolean('completed_initial_receipt')->default(false);
            $table->boolean('helper')->default(true);
            $table->boolean('receiver')->default(false);
            $table->boolean('has_recycled')->default(false);
            $table->boolean('can_recycle')->default(false);
            $table->integer('help_count')->default(0);
            $table->integer('received_count')->default(0);
            $table->boolean('confirm_me')->default(false);
            $table->boolean('has_donor')->default(false);
            $table->boolean('is_recycling')->default(false);
            $table->boolean('completed_cycle')->default(false);
            $table->boolean('awaiting_donor')->default(false);
            $table->boolean('awaiting_payment_confirmation')->default(false);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}