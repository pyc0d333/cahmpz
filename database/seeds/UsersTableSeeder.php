<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        DB::table('users')->delete();

        DB::table('users')->insert([

//            [
//
//                'id'=>1,
//                'name' => 'All is Well',
//                'phone' => '00105489641',
//                'email' => 'alliswell2870@gmail.com',
//                'given_help' => true,
//                'awaiting_help' => true,
//                'package_id'=>0,
//                'acct_name'=> 'Testing',
//                'account_number' => '7089256464',
//                'bank_name' => 'FIDELITY BANK',
//                'is_superadmin'=>true,
//                'has_pin' =>true,
//                'password' => bcrypt('alliswell2870@gmail.com'),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//            ],
//            [
//                'id'=>2,
//                'name' => 'Madam',
//                'phone' => '00105489641',
//                'email' => 'nancyoge35@gmail.com',
//                'given_help' => true,
//                'awaiting_help' => true,
//                'package_id'=>0,
//                'acct_name'=> 'Testing',
//                'is_superadmin'=>true,
//                'has_pin' =>true,
//                'account_number' => '7089256464',
//                'bank_name' => 'FIDELITY BANK',
//                'password' => bcrypt('nancyoge35@gmail.com'),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//            ],

//            [
//                'name' => 'Tolulope Morufat Adams',
//                'phone' => '09082050026',
//                'package_id'=>4,
//                'email' => 'anitatolu@gmail.com',
//                'acct_name'=> 'Tolulope Adam',
//                'account_number' => '1763307363',
//                'bank_name' => 'Skye Bank',
//                'is_superadmin'=>true,
//                'has_pin' =>true,
//                'given_help' => true,
//                'awaiting_help' => true,
//                'password' => bcrypt('anitatolu@gmail.com'),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//            ],
//            [
//                'name' => 'Promise Chibuikem',
//                'phone' => '08087238319',
//                'account_number' => '0086207301',
//                'bank_name' => 'DIAMOND BANK',
//                'email' => 'promiseonyegbunajnr@gmail.com',
//                'acct_name'=> 'Promise Chibuikem',
//                'has_pin' =>true,
//                'given_help' => true,
//                'package_id'=>3,
//                'awaiting_help' => true,
//                'password' => bcrypt('promiseonyegbunajnr@gmail.com'),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//            ],
                        [
                'name' => 'Prince Alochuks Logistics Ltd',
                            'phone' => '09038118911',
                            'account_number' => '0025673336',
                            'bank_name' => 'DIAMOND BANK',
                            'acct_name' => 'Prince Alochuks Logistics Ltd',
                'email' => 'ebychyn@yahoo.com',
                            'is_admin' => true,
                            'has_pin' =>true,
                            'given_help' => true,
                            'package_id'=>5,
                            'awaiting_help' => true,
                'password' => bcrypt('ebychyn@yahoo.com'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

        ]);
    }
}
