<?php

use Illuminate\Database\Seeder;

class PinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pins')->delete();

        DB::table('pins')->insert([

            ['pin'=>129303,
                'owner_id'=> 0,
                'id'=>1,
            ],

        ]);
    }
}
