<?php

use Illuminate\Database\Seeder;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */

    public function run()
    {
        DB::table('packages')->delete();

        DB::table('packages')->insert([

//            ['amount'=>1000000,
//              'id'=>1
//            ],
//            ['amount'=>500000,
//              'id'=>2
//            ],

            ['amount'=>200000,
              'id'=>3
            ],
            ['amount'=>100000,
              'id'=>4
            ],
            ['amount'=>50000,
              'id'=>5
            ]
        ]);
    }
}
