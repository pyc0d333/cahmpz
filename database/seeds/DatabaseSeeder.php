<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = \Hash::make('123456');
         DB::table('cms_users')->insert(array(
            'id'                =>DB::table('cms_users')->max('id')+1,
            'created_at'        =>date('Y-m-d H:i:s'),
            'name'              => 'Super Admin',
            'email'             => 'admin@wapfunds.com',
            'password'          => $password,
            'id_cms_privileges' => 1,
            'status'            =>'Active'
        ));
//         $this->call(UsersTableSeeder::class);
//         $this->call(PackageTableSeeder::class);
//         $this->call(BankTableSeeder::class);
//         $this->call(PinTableSeeder::class);
    }
}
