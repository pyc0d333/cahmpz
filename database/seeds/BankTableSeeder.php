<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->delete();

        DB::table('banks')->insert([

            [
                'name' => 'UNION BANK OF NIGERIA PLC'
                ],

            [
                'name' => 'ACCESS BANK PLC'
                ],

            [
                'name' => 'DIAMOND BANK PLC'
                ],

            [
                'name' => 'FIDELITY BANK PLC'
                ],

            [
                'name' => 'FIRST CITY MONUMENT BANK PLC'
                ],

            [
                'name' => 'FIRST BANK NIGERIA LIMITED'
                ],

            [
                'name' => 'GUARANTY TRUST BANK PLC'
                ],

            [
                'name' => 'SKYE BANK PLC'
                ],

            [
                'name' => 'UNITED BANK OF AFRICA PLC'
                ],
            [
                'name' => 'ZENITH BANK PLC'
                ],
            [
                'name' => 'CITIBANK NIGERIA LIMITED'
                ],
            [
                'name' => 'HERITAGE BANK LIMITED'
                ],
            [
                'name' => 'KEYSTONE BANK LIMITED'
                ],
            [
                'name' => 'STANBIC IBTC BANK PLC'
                ],
            [
                'name' => 'STANDARD CHARTERED BANK LIMITED'
                ],
            [
                'name' => 'STERLING BANK PLC'
                ],
            [
                'name' => 'UNITY BANK PLC'
                ],

            [
                'name' => 'WEMA BANK PLC'
                ]



        ]);

    }
}
