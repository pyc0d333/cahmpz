<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'SiteController@home')->name('dashboard-home');

Route::get('', function () {
    return view('welcome');
});



Route::get('/profile', 'SiteController@profile')->name('profile');
Route::get('/payment_info', 'SiteController@payment_info')->name('pay_info');

Route::get('/info', 'SiteController@info')->name('info');
Route::get('/ticket', 'TicketController@index')->name('ticket');
Route::get('/awaiting_donor', 'SiteController@awaiting_donor')->name('awaiting_donor');


Route::get('/onboarding', 'UserController@add_pin')->name('token_page');
Route::post('/add_token', 'UserController@validatePin')->name('add_token');



Route::get('/sponsor', 'SiteController@sponsor')->name('sponsor');

Route::get('/confirm-payment', function (){
    return view('dashboard.confirm-payment');
});


Route::get('/complete_help_success', function (){
    return view('partials.complete_help_success')->name('complete_help_success');
});


Route::get('/earnings', function (){
    return view('dashboard.earnings');
});

Route::get('/provide_help_two', 'SiteController@provide_help_two')->name('provide_help_two');

Route::get('/matches', 'SiteController@matches')->name('matches');
Route::get('/awaiting_payment_confirmation', 'SiteController@awaiting_payment_confirmation')->name('awaiting_payment_confirmation');

Auth::routes();

Route::post('/send', 'EmailController@send');

//Route::post('/provide_help/{id}', 'ProvideHelp@provideHelp')->name('provide_help');
Route::post('/get_help/{id}', 'OrderController@getHelp')->name('get_help');
Route::post('/payment_info/{id}', 'UserController@paymentInfo')->name('add_payment_info');
Route::post('/provide_help_confirmation/{id}', 'OrderController@provider_payment_confirmation_request')->name('payment_confirmation_request');
Route::post('/confirm_provider_payment/{id}/{userId}', 'OrderController@confirm_provider_payment')->name('confirm_provider_payment');
Route::post('/confirm_provider_payment_r/{id}/{userId}', 'OrderController@confirm_provider_payment_r')->name('confirm_provider_payment_r');


Route::post('/ticket/{id}/', 'TicketController@save')->name('add_ticket');



Route::post('/provide-help/{id}', 'OrderController@save')->name('new_order');
Route::post('/provide-help/recycle_account/{id}', 'OrderController@recycle')->name('recycle_order');



/**
 * Admin Routes
 */

Route::get('/admin/users', 'SiteController@list_users')->name('list-users');
Route::get('/admin/add_user', 'SiteController@add_user')->name('add_user');
Route::get('/admin/update_user/{id}', 'SiteController@update_user_view')->name('update_user_view');
Route::get('/admin/user_details/{id}', 'SiteController@user_details_view')->name('user_details_view');


Route::post('/admin/add_user', 'UserController@add_user')->name('add-user');
Route::post('/admin/update_user/{id}', 'UserController@update_user')->name('update-user');
Route::post('/admin/delete_user/{id}', 'UserController@delete_user')->name('delete-user');


Route::get('/banks', 'BankController@index')->name('banks');
Route::post('/add_bank', 'BankController@save')->name('add_bank');
Route::post('/delete_bank/{id}', 'BankController@delete')->name('delete_bank');
Route::get('/get_bank/{id}', 'BankController@view')->name('get_bank');


Route::get('/packages', 'PackageController@index')->name('packages');
Route::post('/add_bank', 'PackageController@save')->name('add_package');


Route::get('/pins', 'PinController@index')->name('pins');
Route::get('/pins/{id}{count}', 'PinController@save')->name('add_pin');


Route::get('/provide_help_requests', 'OrderController@provide_help_requests')->name('provide_help_requests');
Route::get('/orders/provide_help_requests/{id}', 'OrderController@view_provide_help_order')->name('view_provide_help_order');
Route::post('/add_sponsor/{id}', 'OrderController@add_sponsor')->name('add_sponsor');
Route::post('/orders/provide_sponsor/{id}', 'OrderController@provide_sponsor')->name('provide_sponsor');



Route::get('/get_help_requests', 'OrderController@get_help_requests')->name('get_help_requests');
Route::get('/orders/get_help_requests/{id}', 'OrderController@view_get_help_order')->name('view_awaiting_help_order');
Route::post('/provide_donor/{id}', 'OrderController@provide_donor')->name('provide_donor');
Route::post('/provide_donor_2/{id}', 'OrderController@provide_2_donors')->name('provide_2_donor');


Route::get('/tickets', 'TicketController@admin_index')->name('list_tickets');
Route::get('/ticket_update/{id}', 'TicketController@details')->name('update_ticket');
Route::get('/tickets_resolved/{id}', 'TicketController@resolve_ticket')->name('resolve_ticket');
