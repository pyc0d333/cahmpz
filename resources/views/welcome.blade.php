<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- Begin Head -->
<head>
    <!-- Basic -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>CHAMPDREAMS</title>


    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

    <!-- site Styles -->
    <link href="{{asset('/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/dist/css/animate.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/site/themify/themify.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/site/scrollbar/scrollbar.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/site/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/site/swiper/swiper.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/site/cubeportfolio/css/cubeportfolio.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/global/global.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
</head>
<!-- End Head -->

<!-- Body -->
<body>

<!--========== HEADER ==========-->
<header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
    <!-- Navbar -->
    <div class="s-header__navbar">
        <div class="s-header__container">
            <div class="s-header__navbar-row">
                <div class="s-header__navbar-row-col">
                    <!-- Logo -->
                    <div class="s-header__logo">
                        <a href="/l" class="s-header__logo-link navbar-left">
                            <img class="s-header__logo-img  s-header__logo-img-default"  height="50px" src="img/logo_dark.png" alt="Logo">
                            <img class="s-header__logo-img s-header__logo-img-shrink" height="50px"  src="img/logo.png" alt="Logo">
                        </a>
                    </div>
                    <!-- End Logo -->
                </div>
                <div class="s-header__navbar-row-col">
                    <!-- Trigger -->
                    <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                        <span class="s-header__trigger-icon"></span>
                        <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                            <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                        </svg>
                    </a>
                    <!-- End Trigger -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Navbar -->

    <!-- Overlay -->
    <div class="s-header-bg-overlay js__bg-overlay">
        <!-- Nav -->
        <nav class="s-header__nav js__scrollbar">
            <div class="container-fluid">
                <!-- Menu List -->
                <ul class="list-unstyled s-header__nav-menu">
                    <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active" href="{{route('login')}}">Login</a></li>
                    <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider " href="{{route('register')}}">Register</a></li>
                </ul>
                <!-- End Menu List -->

                <!-- Menu List -->
                {{--<ul class="list-unstyled s-header__nav-menu">--}}
                    {{--<li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active" href="{{route('login')}}">Login</a></li>--}}
                    {{--<li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider " href="{{route('login')}}">Register</a></li>--}}
                {{--</ul>--}}
                <!-- End Menu List -->
            </div>
        </nav>
        <!-- End Nav -->





        <!-- Action -->
        <ul class="list-inline s-header__action s-header__action--lb">
            <li class="s-header__action-item"><a class="s-header__action-link -is-active" href="#">En</a></li>
            <li class="s-header__action-item"><a class="s-header__action-link" href="#">Fr</a></li>
        </ul>
        <!-- End Action -->

        <!-- Action -->
        <ul class="list-inline s-header__action s-header__action--rb">
            <li class="s-header__action-item">
                <a class="s-header__action-link" href="#">
                    <i class="g-padding-r-5--xs ti-facebook"></i>
                    <span class="g-display-none--xs g-display-inline-block--sm">Facebook</span>
                </a>
            </li>
            <li class="s-header__action-item">
                <a class="s-header__action-link" href="#">
                    <i class="g-padding-r-5--xs ti-twitter"></i>
                    <span class="g-display-none--xs g-display-inline-block--sm">Twitter</span>
                </a>
            </li>
            <li class="s-header__action-item">
                <a class="s-header__action-link" href="#">
                    <i class="g-padding-r-5--xs ti-instagram"></i>
                    <span class="g-display-none--xs g-display-inline-block--sm">Instagram</span>
                </a>
            </li>
        </ul>
        <!-- End Action -->
    </div>
    <!-- End Overlay -->
</header>
<!--========== END HEADER ==========-->

<!--========== SWIPER SLIDER ==========-->
<div class="s-swiper js__swiper-one-item">
    <!-- Swiper Wrapper -->
    <div class="swiper-wrapper">
        <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('{{asset('img/1920x1080/15.PNG')}}');">
            <div class="container g-text-center--xs g-ver-center--xs">
                <div class="g-margin-b-30--xs">
                    {{--<h1 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">An Online  Experience<br>That Inspires You To Dare To Achieve</h1>--}}
                </div>
                {{--<a class="js__popup__youtube" href="https://www.youtube.com/watch?v=lcFYdgZKZxY" title="Intro Video">--}}
                    {{--<i class="s-icon s-icon--lg s-icon--white-bg g-radius--circle ti-control-play"></i>--}}
                {{--</a>--}}
            </div>
        </div>
        <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url({{asset('img/1920x1080/04.jpg')}});">
            <div class="container g-text-center--xs g-ver-center--xs">
                <div class="g-margin-b-30--xs">
                    <div class="g-margin-b-30--xs">
                        <h2 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">We have crafted a platform<br>That Helps You<br>Stand Out</h2>
                    </div>
                    {{--<a class="js__popup__youtube" href="https://www.youtube.com/watch?v=lcFYdgZKZxY" title="Intro Video">--}}
                        {{--<i class="s-icon s-icon--lg s-icon--white-bg g-radius--circle ti-control-play"></i>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- End Swiper Wrapper -->

    <!-- Arrows -->
    <a href="javascript:void(0);" class="s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
    <a href="javascript:void(0);" class="s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
    <!-- End Arrows -->

    <a href="#js__scroll-to-section" class="s-scroll-to-section-v1--bc g-margin-b-15--xs">
        <span class="g-font-size-18--xs g-color--white ti-angle-double-down"></span>
        <p class="text-uppercase g-color--white g-letter-spacing--3 g-margin-b-0--xs">Learn More</p>
    </a>
</div>
<!--========== END SWIPER SLIDER ==========-->



<!--========== PAGE CONTENT ==========-->
<!-- Features -->
<div id="js__scroll-to-section" class="container g-padding-y-80--xs g-padding-y-125--sm">
    <div class="g-text-center--xs g-margin-b-100--xs">
        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Welcome to Champion Dreams</p>
        <h2 class="g-font-size-32--xs g-font-size-36--md">We Create Beautiful Experiences <br> That Make Dreams Come True.</h2>
    </div>
    <div class="row g-margin-b-60--xs g-margin-b-70--md">
        <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
            <div class="clearfix">
                <div class="g-media g-width-30--xs">
                    <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                        <i class="g-font-size-28--xs g-color--primary ti-desktop"></i>
                    </div>
                </div>
                <div class="g-media__body g-padding-x-20--xs">
                    <h3 class="g-font-size-18--xs">24/7 Support</h3>
                    <p class="g-margin-b-0--xs">This is where we sit down, grab a cup of coffee and wait for you to contact us.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
            <div class="clearfix">
                <div class="g-media g-width-30--xs">
                    <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".2s">
                        <i class="g-font-size-28--xs g-color--primary ti-server"></i>
                    </div>
                </div>
                <div class="g-media__body g-padding-x-20--xs">
                    <h3 class="g-font-size-18--xs">Highly Secured Platform</h3>
                    <p class="g-margin-b-0--xs">We place a huge emphasis on security of the platform. All your information is kept safe from the wrong hands.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="clearfix">
                <div class="g-media g-width-30--xs">
                    <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".3s">
                        <i class="g-font-size-28--xs g-color--primary ti-reload"></i>
                    </div>
                </div>
                <div class="g-media__body g-padding-x-20--xs">
                    <h3 class="g-font-size-18--xs">Auto-Recycling</h3>
                    <p class="g-margin-b-0--xs">Provide Help, Get one help and provide help automatically to get another help. No Hassles</p>
                </div>
            </div>
        </div>
    </div>
    <!-- // end row  -->
    <div class="row">
        <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
            <div class="clearfix">
                <div class="g-media g-width-30--xs">
                    <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".4s">
                        <i class="g-font-size-28--xs g-color--primary ti-package"></i>
                    </div>
                </div>
                <div class="g-media__body g-padding-x-20--xs">
                    <h3 class="g-font-size-18--xs">Endless Possibilities</h3>
                    <p class="g-margin-b-0--xs">With the packages provided on Champdreams, the possibilities are endless.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
            <div class="clearfix">
                <div class="g-media g-width-30--xs">
                    <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".5s">
                        <i class="g-font-size-28--xs g-color--primary ti-infinite"></i>
                    </div>
                </div>
                <div class="g-media__body g-padding-x-20--xs">
                    <h3 class="g-font-size-18--xs">Instant Matching</h3>
                    <p class="g-margin-b-0--xs">We have brought down the matching time to a very low level. Get matched in no time after you join</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="clearfix">
                <div class="g-media g-width-30--xs">
                    <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".6s">
                        <i class="g-font-size-28--xs g-color--primary ti-panel"></i>
                    </div>
                </div>
                <div class="g-media__body g-padding-x-20--xs">
                    <h3 class="g-font-size-18--xs">Invites Only</h3>
                    <p class="g-margin-b-0--xs">This an invites only platform, this ensures that all the people invited to the platform are capable and serious about making their dreams come true.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- // end row  -->
</div>
<!-- End Features -->



<!-- Parallax -->
<div class="js__parallax-window" style="background: url({{asset('img/1920x1080/03.jpg')}}) 50% 0 no-repeat fixed;">
    <div class="container g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
        <div class="g-margin-b-80--xs">
            <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white">The Fastest Way to Achieve your dreams </h2>
        </div>
        <a href="{{route('register')}}" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50">Learn More</a>
    </div>
</div>
<!-- End Parallax -->
<!-- Culture -->
<div class="g-promo-section">
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="row">
            <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Culture</p>
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                    <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md">About</h2>
                </div>
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                    <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md">Champion Dreams</h2>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <p class="g-font-size-18--xs">We aim high at being focused on building relationships within the  community. Using our creative gifts drives this foundation. The time has come to bring those ideas and plans to life. This is where we really begin to visualize your imaginations and make them into beautiful realities.</p>
                <p class="g-font-size-18--xs">Now that you're dressed up and ready to party, it's time to release it to the world. By the way, let's celebrate already.</p>
            </div>
        </div>
    </div>
    <div class="col-sm-3 g-promo-section__img-right--lg g-bg-position--center g-height-100-percent--md js__fullwidth-img">
        <img class="img-responsive" src="{{asset('img/970x970/03.jpg')}}" alt="Image">
    </div>
</div>
<!-- End Culture -->


<!-- Process -->
<div class="g-bg-color--primary-ltr">
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="g-text-center--xs g-margin-b-100--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Process</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md g-color--white">How it Works</h2>
        </div>
        <ul class="list-inline row g-margin-b-100--xs">
            <!-- Process -->
            <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                <div class="center-block g-text-center--xs">
                    <div class="g-margin-b-30--xs">
                        <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">01</span>
                    </div>
                    <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--white">Get a Registration Token</h3>
                        <p class="g-color--white-opacity">You will be invited with a special token to be used for accessing the platform.</p>
                    </div>
                </div>
            </li>
            <!-- End Process -->

            <!-- Process -->
            <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                <div class="center-block g-text-center--xs">
                    <div class="g-margin-b-30--xs">
                        <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">02</span>
                    </div>
                    <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--white">Provide Help</h3>
                        <p class="g-color--white-opacity">Select a package and request to provide help. A sponsor will be matched to you and you provide help to them.</p>
                    </div>
                </div>
            </li>
            <!-- End Process -->

            <!-- Process -->
            <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--sm">
                <div class="center-block g-text-center--xs">
                    <div class="g-margin-b-30--xs">
                        <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">03</span>
                    </div>
                    <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--white">Request for Help </h3>
                        <p class="g-color--white-opacity">After providing help, ask for help and get matched to 2 people. Receive help, provide help again to release the other funds</p>
                    </div>
                </div>
            </li>
            <!-- End Process -->

            <!-- Process -->
            <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1">
                <div class="center-block g-text-center--xs">
                    <div class="g-margin-b-30--xs">
                        <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">04</span>
                    </div>
                    <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--white">Keep Providing and Receiving</h3>
                        <p class="g-color--white-opacity">You will be auto-recycled so you can keep giving and providing help.</p>
                    </div>
                </div>
            </li>
            <!-- End Process -->
        </ul>

        <div class="g-text-center--xs">
            <a href="{{route('register')}}" class="text-uppercase s-btn s-btn--md s-btn--white-bg g-radius--50 g-padding-x-70--xs">Sign Up</a>
        </div>
    </div>
</div>
<!-- End Process -->

<!-- Subscribe -->
<div class="js__parallax-window" style="background: url({{asset('img/1920x1080/07.jpg')}}) 50% 0 no-repeat fixed;">
    <div class="g-container--sm g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
        <div class="g-margin-b-80--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Subscribe</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md g-color--white">Join Over 10000+ People</h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                <a  href="{{route('register')}}" class="s-btn s-btn-icon--md s-btn-icon--white-brd s-btn--white-brd g-radius--right-50">Sign Up Today<i class="ti-arrow-right"></i></a>

            </div>
        </div>
    </div>
</div>
<!-- End Subscribe -->
<!-- Plan -->
<div class="g-bg-color--sky-light">
    <div class="container g-padding-y-80--xs g-padding-y-125--xsm">
        <div class="g-text-center--xs g-margin-b-80--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Plan</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md">So Many Packages To Choose From</h2>
        </div>

        <div class="row g-row-col--5">
            <!-- Plan -->
            <div class="col-md-4 g-margin-b-10--xs g-margin-b-0--lg">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
                    <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                        <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-archive"></i>
                        <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">Gold</h3>
                        <div class="g-margin-b-40--xs">
                            <span class="s-plan-v1__price-tag">50,000</span>
                        </div>
                        <a  href="{{route('register')}}" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Signup</a>
                    </div>
                </div>
            </div>
            <!-- End Plan -->

            <!-- Plan -->
            <div class="col-md-4 g-margin-b-10--xs g-margin-b-0--lg">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                    <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                        <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-hand-drag"></i>
                        <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">DIAMOND</h3>
                        <div class="g-margin-b-40--xs">
                            <span class="s-plan-v1__price-tag">100,000</span>
                        </div>
                        <a  href="{{route('register')}}" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Signup</a>
                    </div>
                </div>
            </div>
            <!-- End Plan -->

            <!-- Plan -->
            <div class="col-md-4">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".3s">
                    <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                        <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-gift"></i>
                        <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">RUBY</h3>
                        <div class="g-margin-b-40--xs">
                            <span class="s-plan-v1__price-tag">200,000</span>
                        </div>
                        <a  href="{{route('register')}}" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Signup</a>
                    </div>
                </div>
            </div>
            <!-- End Plan -->
        </div>
    </div>
</div>
<!-- End Plan --><!-- Plan -->
<div class="g-bg-color--sky-light">
    <div class="container g-padding-y-80--xs g-padding-y-125--xsm">

        <div class="row g-row-col--5">
            <!-- Plan -->
            <!-- Plan -->
            {{--<div class="col-md-4 col-offset-md-2">--}}
                {{--<div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".3s">--}}
                    {{--<div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">--}}
                        {{--<i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-control-stop"></i>--}}
                        {{--<h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">EMERALD</h3>--}}
                        {{--<div class="g-margin-b-40--xs">--}}
                            {{--<span class="s-plan-v1__price-tag">500,000</span>--}}
                        {{--</div>--}}
                        {{--<a  href="{{route('register')}}" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Signup</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- End Plan -->--}}
            <!-- Plan -->
            {{--<div class="col-md-4">--}}
                {{--<div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".3s">--}}
                    {{--<div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">--}}
                        {{--<i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-star"></i>--}}
                        {{--<h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">PLATINUM</h3>--}}
                        {{--<div class="g-margin-b-40--xs">--}}
                            {{--<span class="s-plan-v1__price-tag">1,000,000</span>--}}
                        {{--</div>--}}
                        {{--<a  href="{{route('register')}}" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Signup</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- End Plan -->
        </div>
    </div>
</div>
<!-- End Plan -->

<!-- Testimonials -->
<div class="js__parallax-window" style="background: url({{asset('img/1920x1080/04.jpg')}}) 50% 0 no-repeat fixed;">
    <div class="container g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-50--xs">Testimonials</p>
        <div class="s-swiper js__swiper-testimonials">
            <!-- Swiper Wrapper -->
            <div class="swiper-wrapper g-margin-b-50--xs">
                <div class="swiper-slide g-padding-x-130--sm g-padding-x-150--lg">
                    <div class="g-padding-x-20--xs g-padding-x-50--lg">
                        <div class="g-margin-b-40--xs">
                            <p class="g-font-size-22--xs g-font-size-28--sm g-color--white"><i>" I have tried many GH/PH platforms but have never really gotten it until I joined Champdreams. "</i></p>
                        </div>
                        <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                        <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--white-opacity-light g-margin-b-5--xs">Jacob Ola / Lagos</h4>
                    </div>
                </div>
                <div class="swiper-slide g-padding-x-130--sm g-padding-x-150--lg">
                    <div class="g-padding-x-20--xs g-padding-x-50--lg">
                        <div class="g-margin-b-40--xs">
                            <p class="g-font-size-22--xs g-font-size-28--sm g-color--white"><i>" Champion Dreams is real. I was able to enjoy my dream break in Dubai "</i></p>
                        </div>
                        <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                        <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--white-opacity-light g-margin-b-5--xs">Olumide / Ibadan </h4>
                    </div>
                </div>
                <div class="swiper-slide g-padding-x-130--sm g-padding-x-150--lg">
                    <div class="g-padding-x-20--xs g-padding-x-50--lg">
                        <div class="g-margin-b-40--xs">
                            <p class="g-font-size-22--xs g-font-size-28--sm g-color--white"><i>" I have only one sentence. "JOIN NOW!!!" "</i></p>
                        </div>
                        <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                        <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--white-opacity-light g-margin-b-5--xs">Musa Ahmed/ Kano</h4>
                    </div>
                </div>
            </div>
            <!-- End Swipper Wrapper -->

            <!-- Arrows -->
            <div class="g-font-size-22--xs g-color--white-opacity js__swiper-fraction"></div>
            <a href="javascript:void(0);" class="g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
            <a href="javascript:void(0);" class="g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
            <!-- End Arrows -->
        </div>
    </div>
</div>
<!-- End Testimonials -->

{{--<!-- Clients -->--}}
{{--<div class="g-bg-color--sky-light">--}}
    {{--<div class="g-container--md g-padding-y-80--xs g-padding-y-125--sm">--}}
        {{--<!-- Swiper Clients -->--}}
        {{--<div class="s-swiper js__swiper-clients">--}}
            {{--<div class="swiper-wrapper">--}}
                {{--<div class="swiper-slide">--}}
                    {{--<div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".1s">--}}
                        {{--<img class="s-clients-v1" src="img/clients/01-dark.png" alt="Clients Logo">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="swiper-slide">--}}
                    {{--<div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".2s">--}}
                        {{--<img class="s-clients-v1" src="img/clients/02-dark.png" alt="Clients Logo">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="swiper-slide">--}}
                    {{--<div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".3s">--}}
                        {{--<img class="s-clients-v1" src="img/clients/03-dark.png" alt="Clients Logo">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="swiper-slide">--}}
                    {{--<div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".4s">--}}
                        {{--<img class="s-clients-v1" src="img/clients/04-dark.png" alt="Clients Logo">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="swiper-slide">--}}
                    {{--<div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".5s">--}}
                        {{--<img class="s-clients-v1" src="img/clients/05-dark.png" alt="Clients Logo">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- End Swiper Clients -->--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!-- End Clients -->--}}

{{--<!-- News -->--}}
{{--<div class="container g-padding-y-80--xs g-padding-y-125--sm">--}}
    {{--<div class="g-text-center--xs g-margin-b-80--xs">--}}
        {{--<p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Blog</p>--}}
        {{--<h2 class="g-font-size-32--xs g-font-size-36--md">Latest News</h2>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-sm-4 g-margin-b-30--xs g-margin-b-0--md">--}}
            {{--<!-- News -->--}}
            {{--<article>--}}
                {{--<img class="img-responsive" src="img/970x970/01.jpg" alt="Image">--}}
                {{--<div class="g-bg-color--white g-box-shadow__dark-lightest-v2 g-text-center--xs g-padding-x-40--xs g-padding-y-40--xs">--}}
                    {{--<p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2">News</p>--}}
                    {{--<h3 class="g-font-size-22--xs g-letter-spacing--1"><a href="{{route('register')}}/">Create Something Great.</a></h3>--}}
                    {{--<p>The time has come to bring those ideas and plans to life.</p>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<!-- End News -->--}}
        {{--</div>--}}
        {{--<div class="col-sm-4 g-margin-b-30--xs g-margin-b-0--md">--}}
            {{--<!-- News -->--}}
            {{--<article>--}}
                {{--<img class="img-responsive" src="img/970x970/02.jpg" alt="Image">--}}
                {{--<div class="g-bg-color--white g-box-shadow__dark-lightest-v2 g-text-center--xs g-padding-x-40--xs g-padding-y-40--xs">--}}
                    {{--<p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2">News</p>--}}
                    {{--<h3 class="g-font-size-22--xs g-letter-spacing--1"><a href="{{route('register')}}/">Jacks of All. Experts in All.</a></h3>--}}
                    {{--<p>The time has come to bring those ideas and plans to life.</p>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<!-- End News -->--}}
        {{--</div>--}}
        {{--<div class="col-sm-4">--}}
            {{--<!-- News -->--}}
            {{--<article>--}}
                {{--<img class="img-responsive" src="img/970x970/03.jpg" alt="Image">--}}
                {{--<div class="g-bg-color--white g-box-shadow__dark-lightest-v2 g-text-center--xs g-padding-x-40--xs g-padding-y-40--xs">--}}
                    {{--<p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2">News</p>--}}
                    {{--<h3 class="g-font-size-22--xs g-letter-spacing--1"><a href="{{route('register')}}/">Finding your Perfect Place.</a></h3>--}}
                    {{--<p>The time has come to bring those ideas and plans to life.</p>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<!-- End News -->--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!-- End News -->--}}

<!-- Counter -->
<div class="js__parallax-window" style="background: url({{asset('img/1920x1080/06.jpg')}}) 50% 0 no-repeat fixed;">
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="row">
            <div class="col-md-3 col-xs-6 g-full-width--xs g-margin-b-70--xs g-margin-b-0--lg">
                <div class="g-text-center--xs">
                    <div class="g-margin-b-10--xs">
                        <figure class="g-display-inline-block--xs g-font-size-70--xs g-color--white js__counter">6</figure>
                        <span class="g-font-size-40--xs g-color--white">m</span>
                    </div>
                    <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                    <h4 class="g-font-size-18--xs g-color--white">worth of transactions</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 g-full-width--xs g-margin-b-70--xs g-margin-b-0--lg">
                <div class="g-text-center--xs">
                    <figure class="g-display-block--xs g-font-size-70--xs g-color--white g-margin-b-10--xs js__counter">10000</figure>
                    <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                    <h4 class="g-font-size-18--xs g-color--white">Users</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 g-full-width--xs g-margin-b-70--xs g-margin-b-0--sm">
                <div class="g-text-center--xs">
                    <figure class="g-display-block--xs g-font-size-70--xs g-color--white g-margin-b-10--xs js__counter">1</figure>
                    <span class="g-font-size-40--xs g-color--white"></span>

                    <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                    <h4 class="g-font-size-18--xs g-color--white">Maximum wait time for matching</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 g-full-width--xs">
                <div class="g-text-center--xs">
                    <div class="g-margin-b-10--xs">
                        <figure class="g-display-inline-block--xs g-font-size-70--xs g-color--white js__counter">100</figure>
                        <span class="g-font-size-40--xs g-color--white">x</span>
                    </div>
                    <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                    <h4 class="g-font-size-18--xs g-color--white">Faster Support</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Counter -->

<!-- Feedback Form -->
<div class="g-bg-color--sky-light">
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="g-text-center--xs g-margin-b-80--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Feedback</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md">Send us a note</h2>
        </div>
        <form>
            <div class="row g-margin-b-40--xs">
                <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md">
                    <div class="g-margin-b-20--xs">
                        <input type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Name">
                    </div>
                    <div class="g-margin-b-20--xs">
                        <input type="email" class="form-control s-form-v2__input g-radius--50" placeholder="* Email">
                    </div>
                    <input type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Phone">
                </div>
                <div class="col-sm-6">
                    <textarea class="form-control s-form-v2__input g-radius--10 g-padding-y-20--xs" rows="8" placeholder="* Your message"></textarea>
                </div>
            </div>
            <div class="g-text-center--xs">
                <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- End Feedback Form -->

<!-- Google Map -->
{{--<section class="s-google-map">--}}
    {{--<div id="js__google-container" class="s-google-container g-height-400--xs"></div>--}}
{{--</section>--}}
<!-- End Google Map -->
<!--========== END PAGE CONTENT ==========-->

<!--========== FOOTER ==========-->
<footer class="g-bg-color--dark">
    <!-- Links -->
    <div class="g-hor-divider__dashed--white-opacity-lightest">
        {{--<div class="container g-padding-y-80--xs">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-2 g-margin-b-20--xs g-margin-b-0--md">--}}
                    {{--<ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Home</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">About</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Work</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Contact</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-sm-2 g-margin-b-20--xs g-margin-b-0--md">--}}
                    {{--<ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Twitter</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Facebook</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Instagram</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">YouTube</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-sm-2 g-margin-b-40--xs g-margin-b-0--md">--}}
                    {{--<ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Subscribe to Our Newsletter</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Privacy Policy</a></li>--}}
                        {{--<li><a class="g-font-size-15--xs g-color--white-opacity" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes">Terms &amp; Conditions</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 s-footer__logo g-padding-y-50--xs g-padding-y-0--md">--}}
                    {{--<h3 class="g-font-size-18--xs g-color--white">Megakit</h3>--}}
                    {{--<p class="g-color--white-opacity">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End Links -->--}}

    <!-- Copyright -->
    <div class="container g-padding-y-50--xs">
        <div class="row">
            <div class="col-xs-6">
                <a href="#">
                    <img class="g-width-100--xs g-height-auto--xs" src="img/logo.png" alt=" Logo">
                </a>
            </div>
            <div class="col-xs-6 g-text-right--xs">
                <p class="g-font-size-14--xs g-margin-b-0--xs g-color--white-opacity-light">Powered by: <a href="#">Champdream.com</a></p>
            </div>
        </div>
    </div>
    </div>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/591346a564f23d19a89b1954/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <!-- End Copyright -->
</footer>
<!--========== END FOOTER ==========-->

<!-- Back To Top -->
<a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

<!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
<!-- site -->
<script type="text/javascript" src="{{asset('site/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/jquery.migrate.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset ('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script><script type="text/javascript" src="{{asset('site/jquery.smooth-scroll.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/jquery.back-to-top.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/scrollbar/jquery.scrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/swiper/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/waypoint.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/counterup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/jquery.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/jquery.wow.min.js')}}"></script>

<!-- General Components and Settings -->

<script type="text/javascript" src="{{asset('js/global.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/header-sticky.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/scrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/swiper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/counter.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/portfolio-3-col.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/google-map.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/components/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('')}}"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U"></script>
<!--========== END JAVASCRIPTS ==========-->

</body>
<!-- End Body -->
</html>