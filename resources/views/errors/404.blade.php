@extends('layouts.auth-layout')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="panel panel-default card-view mb-0">
                        <div class="panel-heading">
                            <div class="pull-left">
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-wrap">
                                            <h6 class="txt-dark">SORRY WHAT YOU'RE REQUESTED COULD NOT BE FOUND</h6>

                                            <a href="{{route('dashboard-home')}}" class="btn btn-success">Back Home?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

    </div>
    <!-- /Main Content -->
@endsection
