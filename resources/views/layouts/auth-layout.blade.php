<!DOCTYPE html>
<html lang="en">
<head>
   @include('partials.head')
   <!-- CSRF Token -->
       <meta name="csrf-token" content="{{ csrf_token() }}">

       <!-- Scripts -->
       <script>
           window.Laravel = <?php echo json_encode([
               'csrfToken' => csrf_token(),
           ]); ?>
       </script>
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0 ">

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0" style="background-color: #081F2C">
        @yield('content')
    </div>

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/591346a564f23d19a89b1954/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<!-- JavaScript -->
@include('partials.scripts')
</body>
</html>