!DOCTYPE html>
<html lang="en">
<head>
 @include('partials.head')
 <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">

     <!-- Scripts -->
     <script>
         window.Laravel = <?php echo json_encode([
             'csrfToken' => csrf_token(),
         ]); ?>
     </script>
</head>

<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->




<div class="wrapper slide-nav-toggle">

    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block mr-20 pull-lef" href="javascript:void(0);"><i class="fa fa-bars"></i></a>
        <a href="#"><img class="brand-img pull-left" height="50px" width="60px" src="{{asset('img/logo_dark.png')}}" alt="champdreams"/></a>
        <div class="collapse navbar-search-overlap" id="site_navbar_search">
            <form role="search">
                <div class="form-group mb-0">
                    <div class="input-search">
                        <div class="input-group">
                            <input type="text" id="overlay_search" name="overlay-search" class="form-control pl-30" placeholder="Search">
                            <span class="input-group-addon pr-30">
									<a  href="javascript:void(0)" class="close-input-overlay" data-target="#site_navbar_search" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="fa fa-times"></i></a>
									</span>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </nav>
    <!-- /Top Menu Items -->

    @include('dashboard.sidebar')


    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

             @yield('content')



        </div>
        <!-- /Main Content -->

    </div>
</div>
    <!-- /#wrapper -->

    <!-- JavaScript -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/591346a564f23d19a89b1954/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
     @include('partials.scripts')

</body>

</html>
