<div class="col-lg-4 col-sm-4 col-xs-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">What To Do</h6>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="streamline">

                    <div class="sl-item sl-danger">
                        <div class="sl-content">
                            <p  class="txt-dark">Provide Another Help to get your second payment</p>
                        </div>
                    </div>


                    <div class="sl-item sl-danger">
                        <div class="sl-content">
                            <div class="alert alert-danger">
                                <p > You Have  an amount of Pending. <span class="label label-success">{{$package->amount}}</span> Recylce to get it back</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>