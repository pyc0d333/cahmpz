<div class="col-lg-8 col-sm-8 col-xs-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">What To Do</h6>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="streamline">
                    <div class="sl-item sl-danger">
                        <div class="sl-content">
                            <div class="alert alert-danger">
                                <p>PLease wait as we look for someone to merge you with</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>