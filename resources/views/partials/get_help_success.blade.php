@extends('layouts.app')
@section('content')
    <div class="col-lg-9 col-sm-9 col-xs-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Thank You</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="streamline">

                        <div class="sl-item sl-danger">
                            <div class="sl-content">
                                <div class="alert alert-danger">
                                    <p > You Have Requested to Get Your first Help on Champdreams. WAIT WHILE WE MERGE YOU </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection