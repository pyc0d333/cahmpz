<div class="col-lg-4 col-sm-4 col-xs-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">What To Do</h6>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="streamline">

                    <div class="sl-item sl-danger">
                        <div class="sl-content">
                            <p  class="txt-dark">Copy Sponsor Account Number which is  <strong class="label label-primary">{{$sponsor->account_number}}</strong></p>
                        </div>
                    </div>

                    <div class="sl-item sl-info">
                        <div class="sl-content">
                            <p  class="txt-dark">Copy Sponsor Account Name which is  <strong class="label label-primary">{{$sponsor->acct_name}}</strong></p>
                        </div>
                    </div>
                    <div class="sl-item sl-primary">
                        <div class="sl-content">
                            <p class="txt-dark">Copy Sponsor Phone number which is  <strong class="label label-primary">{{$sponsor->phone}}</strong></p>
                        </div>
                    </div>

                    <div class="sl-item sl-success">
                        <div class="sl-content">
                            <p  class="txt-dark">Call the Sponsor to confirm that the information supplied is valid</p>
                        </div>
                    </div>

                    <div class="sl-item sl-warning">
                        <div class="sl-content">
                            <p  class="txt-dark">Make payment and call sponsor to confirm.</p>
                            <p class="alert alert-danger">You have 5 hours from time of getting sponsor to make payment or risk getting your account deleted</p>
                        </div>
                    </div>

                    <div class="sl-item sl-warning">
                        <div class="sl-content">
                            <p  class="txt-dark">After making Payment, call sponsor to confirm and then click on the Confirm Payment Button</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>