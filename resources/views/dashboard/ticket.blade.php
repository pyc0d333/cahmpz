@extends('layouts.app')
@section('content')
    <?php
    $tickets= App\Ticket::where('user_id', Auth::user()->id)->get();

    ?>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-2">
            <div class="panel panel-default card-view">
                @include('dashboard.notification')

                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Submit a ticket with your complaints and we will get back to you shortly</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('message'))
                            <div class="alert-success">{{session('message')}}</div>
                        @endif
                        <div class="form-wrap">
                            <form class="form-group" name="form" method="post" action="{{route('add_ticket', Auth::user()->id)}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label mb-10" for="email_de">MESSAGE</label>
                                    <textarea type="text" name="message"  class="form-control"  id="email_de" maxlength="500" required></textarea>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-angle-double-up"></i><span class="btn-text">Submit Ticket</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /Row -->
@endsection()
