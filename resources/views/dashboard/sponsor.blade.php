@extends('layouts.app')
@section('content')

    <?php
    $sponsor = App\User::find(Auth::user()->sponsor);
    ?>
    <div class="row">
        <div>
            <p class="alert alert-danger">You have 7hrs to pay to your sponsors account or lose your space</p>
        </div>
        <div class="col-lg-5 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Your Sponsor Information</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p>ACCOUNT NAME</p>
                                    <p class="txt-dark"><strong>{{$sponsor->acct_name}}</strong></p>
                                </div>
                            </div>

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <p>ACCOUNT NUMBER</p>
                                    <p  class="txt-dark"><strong>{{$sponsor->account_number}}</strong></p>
                                </div>
                            </div>

                            <div class="sl-item sl-success">
                                <div class="sl-content">
                                    <p>BANK </p>
                                    <p  class="txt-dark"><strong>{{$sponsor->bank_name}}</strong></p>
                                </div>
                            </div>

                            <div class="sl-item sl-warning">
                                <div class="sl-content">
                                    <p>PHONE NUMBER</p>
                                    <p  class="txt-dark "><strong>{{$sponsor->phone}}</strong></p>
                                </div>
                            </div>

                            <a href="{{route('matches')}}" class="btn btn-success">Go and Confirm Payment</a>
                            {{--<form  method="post" action="{{route('payment_confirmation_request', $user->id)}}">--}}
                                {{--{{csrf_field()}}--}}
                                {{--<button type="submit" class="btn btn-large btn-primary btn-rounded">Confirm Payment</button>--}}
                            {{--</form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view bg-dark">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-light">What To Do</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p class="txt-dar">Copy Sponsor Payment Details</p>
                                </div>
                            </div>

                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p  class="txt-dak">Call Sponsor to confirm account information</p>
                                </div>
                            </div>

                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p  class="txt-dar">Make payment and call sponsor to confirm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /Row -->
@endsection()