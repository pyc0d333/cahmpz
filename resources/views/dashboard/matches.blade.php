@extends('layouts.app')
@section('content')

    <?php

    $user = App\User::find(Auth::user()->id);
    $left = App\User::find($user->left);
    $right = App\User::find($user->right);
    $sponsor = App\User::find($user->sponsor);

    ?>

    <div class="row">
        <div class="col-sm-6">
            @include('dashboard.notification')

        </div>

    </div>

    @if($user->awaiting_donor)
        <h4>Please wait while we merge you</h4>

        @else
    @endif

    @if($left == 0 && $right == 0 && $user->sponsor != 0 )

        <div class="row">
            <div class="alert alert-danger"><h6>You are currently not receiving help. Here is your current sponsor info if you need to make any contacts</h6></div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Sponsor</h6>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body task-panel">

                            <div class="list-group mb-0">
                                <ul>
                                    <li href="#" class="list-group-item">
                                        <span class="label label-info pull-right"> {{$sponsor->name}}</span>
                                        <i class="fa fa-user"></i>Name:

                                    </li>
                                    <li href="#" class="list-group-item">
                                        <span class="label label-primary pull-right">{{$sponsor->email}}</span>
                                        <i class="fa fa-inbox"></i> Email:

                                    </li>
                                    <li href="#" class="list-group-item">
                                        <span class="label label-primary pull-right">{{$sponsor->phone}}</span>
                                        <i class="fa fa-mobile-phone"></i> Phone:

                                    </li>
                                    <li href="#" class="list-group-item">
                                        <span class="label label-primary pull-right">{{$sponsor->account_number}}</span>
                                        <i class="fa fa-sort-numeric-asc"></i> Account Number:

                                    </li>
                                    <li href="#" class="list-group-item">
                                        <span class="label label-primary pull-right">{{$sponsor->acct_name}}</span>
                                        <i class="fa fa-bank"></i> Account Name:

                                    </li>
                                    <li href="#" class="list-group-item">
                                        <span class="label label-primary pull-right">{{$sponsor->bank_name}}</span>
                                        <i class="fa fa-bank"></i> Bank:

                                    </li>
                                </ul>

                                <form  method="post" action="{{route('payment_confirmation_request', $user->id)}}">
                                    {{csrf_field()}}
                                    @if(!$user->confirm_me)

                                        <button type="submit" class="btn btn-large btn-primary btn-rounded">Confirm Payment</button>
                                    @else
                                        <span class="label label-info">You have already requested for payment confirmation</span>
                                        {{--<br><br> <button type="submit" class="btn btn-large btn-primary btn-rounded" disabled>Confirm Payment</button>--}}
                                    @endif
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @elseif($user->sponsor != 0)

                <div class="alert alert-danger"><h6>You currently have no donors or sponsor. Take Action Today</h6></div>
                @else
            @endif

        @if($left != 0)
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body contact-card">
                                <a href="#">
                                    <div class="text-center">
                                        <span class="block card-user-name mt-10">{{$left->name}}</span>
                                        <span class="block card-user-email mt-10">{{$left->email}}</span>
                                        <span class="block card-user-phone mt-5 mb20">{{$left->phone}}</span>

                                        <p class="inline-block">
                                            <form  method="post" action="{{route('confirm_provider_payment', [$left->id, $user->id])}}">
                                                {{csrf_field()}}
                                                @if($left->confirm_me)
                                                    <span class="label label-warning">This Donor has  requested for payment confirmation</span>
                                                    <br><br>
                                                    <button type="submit" class="btn btn-large btn-primary btn-rounded">Confirm Payment</button>
                                                @else
                                                    <div class="row">
                                        <p class="alert alert-info"><span class="label label-primary">{{$left->name}}</span>  has not requested  for payment confirmation</p>
                                        <br>
                                        <p class="alert alert-info">Contact him/her on <strong class="label label-primary">{{$left->phone}} </strong>  to stay updated</p>
                                    </div>

                                    {{--<button type="submit" class="btn btn-large btn-primary btn-rounded" disabled>Confirm Payment</button>--}}
                                    @endif
                                    </form>
                                    </span>
                                </a>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>



@endif

        @if($right != 0)

        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">

                            <a href="#">
                                <div class="text-center">
                                    <span class="block card-user-name mt-10">{{$right->name}}</span>
                                    <span class="block card-user-email mt-10">{{$right->email}}</span>
                                    <span class="block card-user-phone mt-5 mb20">{{$right->phone}}</span>

                                    <span class="inline-block">
                                <form  method="post" action="{{route('confirm_provider_payment_r', [$right->id, $user->id])}}">
                                {{csrf_field()}}
                                    @if($right->confirm_me)
                                        <span class="label label-warning">This Donor has  requested for payment confirmation</span>
                                        <br><br>
                                        <button type="submit" class="btn btn-large btn-primary btn-rounded">Confirm Payment</button>
                                    @else
                                        <div class="row">
                                        <p class="alert alert-info"><span class="label label-primary">{{$right->name}}</span>  has not requested  for payment confirmation</p>
                                        <br>
                                        <p class="alert alert-info">Contact him/her on <strong class="label label-primary">{{$right->phone}} </strong>  to stay updated</p>
                                    </div>

                                        {{--<button type="submit" class="btn btn-large btn-primary btn-rounded" disabled>Confirm Payment</button>--}}
                                    @endif
                                 </form>
                            </span>
                                </div>
                            </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
        @endif
    <!-- /Row -->
@endsection()