@extends('layouts.app')
@section('content')

    @if(Auth::user()->awaiting_donor != true && Auth::user()->receiver)

        <div class="row">
            <div class="col-sm-6">
                @include('dashboard.notification')
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div    class="panel-body contact-card">
                            @if(session('message'))
                                <div class="alert alert-success">{{session('message')}}</div>
                            @endif
                            <p class="alert alert-success">Click on the get help button to get help on Champdreams</p>

                            <form action="{{route('get_help', Auth::user()->id)}}" method="post">
                                {{ csrf_field() }}
                                @if(Auth::user()->awaiting_donor == true)
                                    <div class="label label-danger">You have already requested to get help. <br>Please wait for 0-5 days to get merged.</div>
                                    <br><br>
                                    <hr>
                                    {{--<div class="text-center">--}}
                                        {{--<button type="submit" class="btn btn-danger btn-anim" disabled><i class="icon-rocket" ></i><span class="btn-text">GET HELP</span></button>--}}
                                    {{--</div>--}}

                                @else
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-danger btn-anim"><i class="icon-rocket"></i><span class="btn-text">GET HELP</span></button>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                    @else
                        <h4 class="alert alert-warning">Please wait patiently, you will be merged soon.</h4>
                    @endif
                </div>
            </div>
            @include('partials.get_help_info')
        </div>
@endsection()