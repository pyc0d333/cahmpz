@extends('layouts.app')
@section('content')

<?php
        use  \Carbon\Carbon;

        if(Auth::user()->is_awaiting_sponsor == true)
            {
                $order = App\Order::where('user_id', Auth::user()->id)->where('type', 'provide_help')->orderBy('created_at', 'desc')->first();
                $order_time = $order->created_at;
                $end = $order_time->addHours(1);
            }

            $packages =App\Package::all();



            $user = App\User::find(Auth::user()->id);
            $sponsor = App\User::find($user->sponsor);
$package = App\Package::where('id', $user->package_id)->first();

if($user->sponsor != 0)
                {
                    $order = App\Order::where('user_id', Auth::user()->id)->where('type', 'provide_help')->first();

                }

                else{
                    $order = App\Order::where('user_id', Auth::user()->id)->where('type', 'provide_help')->first();

                }
            if($order != null)
                {
                    $time_to_pay = $order->updated_at->addHours(5)->diffForHumans();

                    if ($order->updated_at->addHours(5)->lte(Carbon::now())) { // ->gte() = greater than or equal
//                          $user->delete();
                    }
                }

        ?>

<div class="row">
    <div class="col-sm-6">
        @include('dashboard.notification')
    </div>
</div>


@if($user->help_count == 0)
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="panel panel-primary card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">
                        <a href="#">
                            <div class="text-center">
                                <p class="label label-success">PROVIDE YOUR FIRST HELP</p>
                                <p><em>select a package and continue</em></p>
                            </div>
                        </a>

                        <form name="form" role="form" method="post" action="{{route ('new_order', Auth::user()->id)}}">
                            {{ csrf_field() }}
                            <div class="form-group mt-30 mb-30">
                                <label class="control-label mb-10 text-left">Package</label>
                                <select class="form-control" name="package" required>
                                    @foreach($packages as $package)
                                        <option value="{{$package->amount}}">{{$package->amount}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-0">
                                @if(Auth::user()->is_awaiting_sponsor)
                                    <button type="submit" class="btn btn-success btn-block btn-anim" disabled><i class="fa fa-user"></i><span class="btn-text">Submit To Get Sponsor</span></button>
                                    <br>
                                @else
                                    <button type="submit" class="btn btn-success btn-block btn-anim"><i class="fa fa-user"></i><span class="btn-text">Submit To Get Sponsor</span></button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @else

    @endif

@if($user->awaiting_donor)

    <h2>Please Wait while we try to match you with a Donor</h2>

    @else
    @endif
    <!-- /Row -->

     @if($user->is_awaiting_sponsor == true)

    @include('dashboard.awaiting_sponsor')

    @else

    @endif

@endsection()
