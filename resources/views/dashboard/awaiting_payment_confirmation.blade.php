
@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-sm-12 col-md-6">
        <h1 class="alert alert-danger">Your Payment is being confirmed, your account will be activated after confirmation</h1>
    </div>
</div>

@endsection