
@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h1 class="alert alert-success">Your request for help has been received. Please wait patiently to be paired with donors.</h1>
        </div>
    </div>

@endsection