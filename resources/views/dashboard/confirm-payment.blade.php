@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            @if(Auth::user()->giving_help == true)
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">
                        <p class="text-center">Click on this button to confirm that you have made the payment to your sponsor</p>
                        <a href="#">
                            <div class="text-center">
                                <button class="btn  btn-success btn-rounded mt-20">CONFIRM PAYMENT</button>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">What To Do</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-success">
                                <div class="sl-content">
                                    <p class="txt-dark">Click on  <span class="label label-success">CONFIRM PAYMENT</span> button to get a match</p>
                                </div>
                            </div>

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <p  class="txt-dark">Call your sponsor to inform them to confirm your payment</p>
                                </div>
                            </div>

                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p  class="txt-dark">Contact Support Incase of any dispute</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @else
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body contact-card">
                            <p class="text-center">Click on this button to confirm that the user has paid</p>
                            <a href="#">
                                <div class="text-center">
                                    <button class="btn  btn-success btn-rounded mt-20">CONFIRM PAYMENT</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">What To Do</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-success">
                                <div class="sl-content">
                                    <p class="txt-dark">Click on  <span class="label label-success">CONFIRM PAYMENT</span> button to get a match</p>
                                </div>
                            </div>

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <p  class="txt-dark">Call your sponsor to inform them to confirm your payment</p>
                                </div>
                            </div>

                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p  class="txt-dark">Contact Support Incase of any dispute</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

    </div>
@endsection()