<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">


{{--        @if(Auth::user()->left == 0  || Auth::user()->right == 0 )--}}


        <li>
            <a   href="{{url('/home')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-home mr-10"></i>Home<span class="pull-right"></span></a>
        </li>
        @if(Auth::user()->is_superadmin == true || Auth::user()->admin == true )


        @if(Auth::user()->is_admin || Auth::user()->left !=0 ||   Auth::user()->right !=0)
        <li>
            <a   href="{{route('matches')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="icon-user mr-10"></i>Matches<span class="pull-right"></span></a>
        </li>
        @else
            @endif
        @if(Auth::user()->giving_help ==  true )
        <li>
            <a   href="{{url('/confirm-payment')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-level-up mr-10"></i>Confirm Payment<span class="pull-right"></span></a>
        </li>
        @else
        @endif

        @else

        @endif
            <li>
                <a   href="{{route('matches')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="icon-user mr-10"></i>Matches<span class="pull-right"></span></a>
            </li>
        <li>
            <a   href="{{url('/info')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-credit-card mr-10"></i>Bank Details<span class="pull-right"></span></a>
        </li>
            <li>
            <a   href="{{url('/ticket')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-signal mr-10"></i>Contact Support<span class="pull-right"></span></a>
        </li>
        {{--<li>--}}
            {{--<a   href="{{url('/earnings')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa  fa-bank mr-10"></i>My Earnings<span class="pull-right"></span></a>--}}
        {{--</li>--}}
        <li>
            <a   href="{{url('/profile')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-universal-access mr-10"></i>My Profile<span class="pull-right"></span></a>
        </li>

        <li>
            <a   href="#" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-universal-access mr-10"></i>Logged in {{Auth::user()->name}}<span class="pull-right"></span></a>
        </li>

        @if(Auth::user()->is_admin)

            <li>
                <a   href="{{url('/packages')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-bar-chart-o mr-10"></i>Packages<span class="pull-right"></span></a>
            </li>
        @else
            @endif


        @if(Auth::user()->is_superadmin)
        <li>
            <a   href="{{route ('banks')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-bank mr-10"></i>Banks<span class="pull-right"></span></a>
        </li>
        <li>
            <a   href="{{ route ('provide_help_requests')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-archive mr-10"></i>Provide Help Requests<span class="pull-right"></span></a>
        </li>
            <li>
            <a   href="{{ route ('get_help_requests')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-archive mr-10"></i>Get Help Requests<span class="pull-right"></span></a>
        </li>

            <li>
            <a   href="{{url('/packages')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-bar-chart-o mr-10"></i>Packages<span class="pull-right"></span></a>
        </li>
        <li>
            <a   href="{{route('list-users')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-users mr-10"></i>Users<span class="pull-right"></span></a>
        </li>
                <li>
            <a   href="{{route('list_tickets')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-ticket mr-10"></i>Support<span class="pull-right"></span></a>
        </li>
        <li>
            <a   href="{{route('pins')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-map-pin mr-10"></i>Generate Pin<span class="pull-right"></span></a>
        </li>
        @else
            @endif
        <li>
            <a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
               <i class="fa fa-sign-out"></i> Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>        </li>
    </ul>
</div>

<!--

