@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-sm-10">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h1 class="panel-title txt-dark"><strong class="label label-primary">User Info</strong></h1>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Package</th>
                                        <th>Time of Request</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        @if($user->is_recycling)
                                        <td><span class="label label-primary">Recycling</span></td>
                                        @else
                                        <td><span class="label label-danger">Not Recycling</span></td>
                                        @endif
                                        <td>{{$package->amount}}</td>
                                        <td>{{$order_time}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">


        @if($user->completed_initial_receipt == 0  ||  $user->is_recycling == false )
        <div class="col-sm-8">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Add Donor</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div class="form-wrap">
                            <form class="form-group" name="form" method="post" action="{{route('provide_donor',$order->id)}}">

                                <label for="select">Choose Donor</label>
                                <select class="form-control" id="select" name="donor" required>
                                    @foreach($users as $donor)
                                        <option name="donor" value="{{$donor->id}}">{{$donor->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-user"></i><span class="btn-text">Add Donor</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @else
                    @endif

                @if($user->completed_initial_receipt && $user->is_recycling)
                    <div class="col-sm-8">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Add Donor</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <h4 class="alert alert-info">Please Do Not Select the same account from the two boxes</h4>
                                    <div class="form-wrap">
                                        <form class="form-group" name="form" method="post" action="{{route('provide_2_donor',$order->id)}}">
{{--                                            {{ csrf_field() }}--}}

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6">

                                                    <label for="select">Choose Donor 1</label>
                                                    <select class="form-control" id="select" name="donor1" required>
                                                        @foreach($users as $donor)
                                                            <option name="donor" value="{{$donor->id}}">{{$donor->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="select2">Choose Donor 2</label>

                                                    <select class="form-control" id="select2" name="donor2" required>
                                                        @foreach($users as $donor)
                                                            <option name="donor" value="{{$donor->id}}">{{$donor->name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                            <br><br><br>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="form-group mb-0">
                                                        <button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-user"></i><span class="btn-text">Add Donors</span></button>
                                                    </div>
                                                </div>
                                            </div>

                                            <br><br>

                                        </form>
                                    </div>
                                </div>
                            </div>
@else

                            @endif
            </div>
        </div>
    </div>

    <!-- /Row -->
@endsection()
