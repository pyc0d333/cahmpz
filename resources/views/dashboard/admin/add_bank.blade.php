@extends('layouts.app')
@section('content')

    <?php
    $banks = App\Bank::all();
        ?>


    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Add Bank</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-wrap">
                            <form class="form-group" name="form" method="post" action="{{url('/add_bank')}}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="control-label mb-10" for="email_de">Bank Name</label>
                                    <input type="text" name="name" value="{{ old('name') }}"  class="form-control"  id="email_de" maxlength="10" required>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-bank"></i><span class="btn-text">Add Bank</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <ul class="list-group">
                   @foreach($banks as $bank)
                  <li class="list-group-item">{{$bank->name}} <span class="pull-right">
                       @endforeach
               </ul>
        </div>
    </div>
    <!-- /Row -->
@endsection()
