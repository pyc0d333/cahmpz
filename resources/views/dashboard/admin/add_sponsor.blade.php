@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-sm-10">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h1 class="panel-title txt-dark"><strong class="label label-primary">User Info</strong></h1>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Package</th>
                                        <th>Time of Request</th>
                                     </tr>
                                    </thead>
                                    <tbody>
                                         <tr>
                                            <td>{{$user->id}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>
                                             @if($user->is_recycling)
                                                 <td><span class="label label-primary">Recycling</span></td>
                                             @else
                                                 <td><span class="label label-danger">Not Recycling</span></td>
                                             @endif
                                            <td>{{$package->amount}}</td>
                                            <td>{{$order_time}}</td>
                                         </tr>
                                     </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>


    <div class="row">

        <div class="col-sm-4">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Add Sponsor</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div class="form-wrap">
                            <form class="form-group" name="form" method="post" action="{{route('provide_sponsor',$order->id)}}">
                                {{ csrf_field() }}

                                <label for="select">Choose Sponsor</label>
                                <select class="form-control" id="select" name="sponsor" required>
                                    <option>------------</option>
                                    @foreach($users as $sponsor)
                                        <option value="{{$sponsor->id}}">{{$sponsor->name}}</option>
                                    @endforeach
                                </select>
                                {{--<div class="form-group mb-0">--}}
                                    {{--<button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-user"></i><span class="btn-text">Assign as Sponsor</span></button>--}}
                                {{--</div>--}}
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- /Row -->
@endsection()
