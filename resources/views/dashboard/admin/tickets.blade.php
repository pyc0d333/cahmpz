@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-sm-10">
            <div class="row">
                <div class="col sm-12">
                    @include('dashboard.notification')

                </div>
            </div>

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h1 class="panel-title txt-dark"><strong class="label label-primary">All Tickets</strong></h1>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Sender</th>
                                        <th>Sender Phone</th>
                                        <th>Sender Email</th>
                                        <th>Ticket No.</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($all as $ticket)
                                        <tr>

                                        <td>{{$ticket->id}}</td>
                                            <?php
                                                $user = App\User::find($ticket->user_id);
                                                    ?>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{$user->email}}</td>

                                            <td>{{$ticket->ticket_no}}</td>
                                            @if($ticket->resolved)
                                        <td><span  class="label label-success">resolved</span></td>
                                              @else
                                                <td> <span class="label label-danger">unresolved</span></td>

                                            @endif
                                           <td>
                                            <span>
                                                <a href="{{route('update_ticket', $ticket->id)}}" style="color:darkblue;">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            {{--<a href="javascript:;" data-toggle="modal" data-target=".delete-modal-{{$ticket->id}}"  style="color:red;">--}}
                                                {{--<i class="fa fa-trash"></i>--}}
                                            {{--</a>--}}
                                            </span>
                                            </td>
                                        </tr>

                                        <div class="col-md-4">

                                            <!-- sample modal content -->
                                            <div class="modal fade delete-modal-{{$ticket->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h5 class="modal-title" id="mySmallModalLabel">Delete User</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div >
                                                                <p class="red-text">Are you sure you want to delete
                                                                    <br><strong class="label label-danger">{{$ticket->name}}</strong>
                                                                    <br> from the system?</p>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-sm-6 col-md-6">

                                                                    <form method="post" action="{{route('delete-user', $ticket->id)}}" >
                                                                        {{ csrf_field() }}
                                                                        <button class="btn btn-block btn-success btn-small" type="submit">Yes</button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-sm-6 col-md-6">
                                                                    <a href="" class="btn btn-danger btn-block" data-dismiss="modal" >No</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                            <!-- Button trigger modal -->
                                        </div>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection()

