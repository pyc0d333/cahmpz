@extends('layouts.app')
@section('content')

    <?php
    $pins = App\Pin::where('owner_id', Auth::user()->id)->orderBy('id', 'desc')->get();
    ?>


    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Generate New Pin</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{--<div class="form-wrap">--}}
                            {{--<form class="form-group" name="form" method="post" action="{{route('add_package')}}">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<div class="form-group mb-0">--}}
                                    {{--<button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-bank"></i><span class="btn-text">Add Package</span></button>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}

                            <a href="{{route('add_pin', [Auth::user()->id, 5])}}" class="btn btn-success btn-amin btn-block">Generate 5 New Pins</a>
                            <a href="{{route('add_pin', [Auth::user()->id, 15])}}" class="btn btn-success btn-amin btn-block">Generate 15  New Pins</a>
                            <a href="{{route('add_pin', [Auth::user()->id, 25])}}" class="btn btn-success btn-amin btn-block">Generate 25 New Pins</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <h5 class="text-center">Latest Pin on top</h5>
            <ul class="list-group">
                @foreach($pins as $pin)
                    <li class="list-group-item">{{$pin->pin}}</li>
                @endforeach
            </ul>
        </div>

        <div class="col-sm-4">
            <h5 class="text-center">Recently Created</h5>
            <ul class="list-group">
                @foreach($pins as $pin)
                    <li class="list-group-item">{{$pin->pin}}</li>
                @endforeach
            </ul>
        </div>
    </div>
    <!-- /Row -->
@endsection()
