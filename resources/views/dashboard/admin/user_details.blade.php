@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">{{$user->name}} Profile</h6>
                </div>
                <div class="clearfix"></div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body task-panel">

                        <div class="list-group mb-0">
                            <ul>
                                <li href="#" class="list-group-item">
                                    <span class="label label-info pull-right"> {{$user->name}}</span>
                                    <i class="fa fa-user"></i>Name:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{$user->email}}</span>
                                    <i class="fa fa-inbox"></i> Email:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{$user->phone}}</span>
                                    <i class="fa fa-mobile-phone"></i> Phone:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{$user->account_number}}</span>
                                    <i class="primary fa fa-credit-card"></i> Account Number:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{$user->bank_name}}</span>
                                    <i class="fa fa-bank"></i> Bank Name:

                                </li>

                                @if($user->is_awaiting_sponsor)
                                    <li href="#" class="list-group-item">
                                        <span class="label label-danger pull-right">Awaiting Sponsor</span>
                                        <i class="fa fa-bank"></i> Sponsor:

                                    </li>

                                @else
                                    <li href="#" class="list-group-item"></li></span>
                                    <i class="fa fa-bank"></i> Sponsor:

                                    </li>
                                @endif

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- /Row -->
@endsection()