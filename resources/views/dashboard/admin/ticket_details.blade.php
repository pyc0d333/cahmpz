@extends('layouts.app')
@section('content')

    <?php
    $packages = App\Package::all();
    $banks = App\Bank::all();
    ?>


    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="list-group mb-0">
                        <ul>
                            <li href="#" class="list-group-item">
                                <span class="label label-info pull-right"> {{$ticket->ticket_no}}</span>
                                <i class="fa fa-sort-numeric-asc"></i>Ticket No:

                            </li>
                            <li href="#" class="list-group-item">
                                <span class="label label-info pull-right"> {{$user->name}}</span>
                                <i class="fa fa-user"></i>User:

                            </li>
                            <li href="#" class="list-group-item">
                                <span class="label label-primary pull-right">{{$user->email}}</span>
                                <i class="fa fa-inbox"></i> Email:

                            </li>
                            <li href="#" class="list-group-item">
                                <span class="label label-primary pull-right">{{$user->phone}}</span>
                                <i class="primary fa-phone"></i> Phone:

                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div>{{$ticket->message}}</div>

                        <a href="{{route('resolve_ticket', $ticket->id)}}" class="btn btn-success btn-anim">Resolved</a>


                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection()
