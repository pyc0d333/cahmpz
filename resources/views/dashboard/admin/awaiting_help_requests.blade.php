@extends('layouts.app')
@section('content')

    <?php

    use \Carbon\Carbon;
    $awaiting_help_orders_fifty = App\Order::where('type', 'awaiting_help')->where('is_completed', false)->where('package_id', 5)->orderBy('created_at', 'asc')->get();
    $awaiting_help_orders_hundred = App\Order::where('type', 'awaiting_help')->where('is_completed', false)->where('package_id', 4)->orderBy('created_at', 'asc')->get();
    $awaiting_help_orders_two_hundred = App\Order::where('type', 'awaiting_help')->where('is_completed', false)->where('package_id', 3)->orderBy('created_at', 'asc')->get();
//    $awaiting_help_orders_five_hundred = App\Order::where('type', 'awaiting_help')->where('is_completed', false)->where('package_id', 2)->orderBy('created_at', 'asc')->get();
//    $awaiting_help_orders_one_million = App\Order::where('type', 'awaiting_help')->where('is_completed', false)->where('package_id', 1)->orderBy('created_at', 'asc')->get();


    ?>


    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h1 class="panel-title txt-dark"><strong class="label label-primary">All 50,000 Get Help Requests</strong></h1>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>User</th>
                                        <th>Time of Request</th>
                                        <th>Assign Donor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($awaiting_help_orders_fifty as $order )
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <?php
                                            $user = App\User::find($order->user_id);
                                            ?>
                                            <td>{{$user->name}}</td>
                                            <td><span class="label label-primary">{{$order->created_at->diffForHumans()}}</span></td>
                                            <td>
                                                <a href="{{route ('view_awaiting_help_order', $order->id)}}" style="color:darkblue;">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h1 class="panel-title txt-dark"><strong class="label label-primary">All 100,000 Get Help Requests</strong></h1>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>User</th>
                                        <th>Time of Request</th>
                                        <th>Assign Donor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($awaiting_help_orders_hundred as $order )
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <?php
                                            $user = App\User::find($order->user_id);
                                            ?>
                                            <td>{{$user->name}}</td>
                                            <td><span class="label label-primary">{{$order->created_at->diffForHumans()}}</span></td>
                                            <td>
                                                <a href="{{route ('view_awaiting_help_order', $order->id)}}" style="color:darkblue;">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <div class="row">
        <div class="col-sm-10">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h1 class="panel-title txt-dark"><strong class="label label-primary">All 200,000 Get Help Requests</strong></h1>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>User</th>
                                        <th>Time of Request</th>
                                        <th>Assign Donor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($awaiting_help_orders_two_hundred as $order )
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <?php
                                            $user = App\User::find($order->user_id);
                                            ?>
                                            <td>{{$user->name}}</td>                                             <td><span class="label label-primary">{{$order->created_at->diffForHumans()}}</span></td>
                                            <td>
                                                <a href="{{route ('view_awaiting_help_order', $order->id)}}" style="color:darkblue;">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-sm-10">--}}
            {{--<div class="panel panel-default card-view">--}}
                {{--<div class="panel-heading">--}}
                    {{--<div class="pull-left">--}}
                        {{--<h1 class="panel-title txt-dark"><strong class="label label-primary">All 500,000 Get Help Requests</strong></h1>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                {{--</div>--}}
                {{--<div class="panel-wrapper collapse in">--}}
                    {{--<div class="panel-body">--}}
                        {{--<div class="table-wrap">--}}
                            {{--<div class="table-responsive">--}}
                                {{--<table id="datable_1" class="table table-hover display  pb-30" >--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>Id</th>--}}
                                        {{--<th>User</th>--}}
                                        {{--<th>Time of Request</th>--}}
                                        {{--<th>Assign Donor</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@foreach($awaiting_help_orders_five_hundred as $order )--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$order->id}}</td>--}}
                                            {{--<?php--}}
                                            {{--$user = App\User::find($order->user_id);--}}
                                            {{--?>--}}
                                            {{--<td>{{$user->name}}</td>                                             <td><span class="label label-primary">{{$order->created_at->diffForHumans()}}</span></td>--}}
                                            {{--<td>--}}
                                                {{--<a href="{{route ('view_awaiting_help_order', $order->id)}}" style="color:darkblue;">--}}
                                                    {{--<i class="fa fa-eye"></i>--}}
                                                {{--</a>--}}
                                            {{--</td>                                        </tr>--}}
                                    {{--@endforeach--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div> <div class="row">--}}
        {{--<div class="col-sm-10">--}}
            {{--<div class="panel panel-default card-view">--}}
                {{--<div class="panel-heading">--}}
                    {{--<div class="pull-left">--}}
                        {{--<h1 class="panel-title txt-dark"><strong class="label label-primary">All 1,000,000 Get Help Requests</strong></h1>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                {{--</div>--}}
                {{--<div class="panel-wrapper collapse in">--}}
                    {{--<div class="panel-body">--}}
                        {{--<div class="table-wrap">--}}
                            {{--<div class="table-responsive">--}}
                                {{--<table id="datable_1" class="table table-hover display  pb-30" >--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>Id</th>--}}
                                        {{--<th>User</th>--}}
                                        {{--<th>Time of Request</th>--}}
                                        {{--<th>Assign Donor</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@foreach($awaiting_help_orders_one_million as $order )--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$order->id}}</td>--}}
                                            {{--<?php--}}
                                            {{--$user = App\User::find($order->user_id);--}}
                                            {{--?>--}}
                                            {{--<td>{{$user->name}}</td>                                             <td><span class="label label-primary">{{$order->created_at->diffForHumans()}}</span></td>--}}
                                            {{--<td>--}}
                                                {{--<a href="{{route ('view_awaiting_help_order', $order->id)}}" style="color:darkblue;">--}}
                                                    {{--<i class="fa fa-eye"></i>--}}
                                                {{--</a>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- /Row -->
    <!-- /Row -->
@endsection()
