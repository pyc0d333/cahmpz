@extends('layouts.app')
@section('content')

    <?php
    $packages = App\Package::all();
    $banks = App\Bank::all();
    ?>


    <div class="row">
        <div class="col sm-6">
            @include('dashboard.notification')

        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Update User Info</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-wrap">
                            <form class="form-horizontal orm-validation mt-20" name="form" role="form" method="POST" action="{{ route('update-user', $user->id) }}">
                                {{ csrf_field() }}
                                <p class="help-block text-left">

                                   Edit User details below:
                                </p>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-3 control-label">Name</label>

                                    <div class="col-md-9">
                                        <input id="name"  type="text" class="form-control underline-input" name="name" value="{{ old('name', $user->name) }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-3 control-label">E-Mail</label>

                                    <div class="col-md-9">
                                        <input id="email" type="email" class="form-control underline-input" name="email" value="{{ old('email', $user->email) }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-3 control-label">Phone</label>

                                    <div class="col-md-9">
                                        <input id="phone"  class="form-control underline-input" name="phone" value="{{ old('phone', $user->phone) }}" required>

                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('is_admin') ? ' has-error' : '' }}">
                                    <label for="is_admin" class="col-md-3 control-label">Admin</label>

                                    <div class="col-md-9">
                                        <input name="is_admin" type="radio" value="{{old(1, $user->is_admin)}}"> yes<br>
                                        <input checked="checked" name="is_admin" type="radio" value="{{old(0, $user->is_admin)}}"> no
                                        @if ($errors->has('is_admin'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('is_admin') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="package" class="col-md-3 control-label">Package</label>
                                    <div class="col-md-9">

                                    <select class="form-control" id="package" name="package" required>
                                        @foreach($packages as $package)
                                            <option name="package" value="{{ old('package_amount', $package->amount)}}">{{$package->amount}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('is_activated') ? ' has-error' : '' }}">
                                    <label for="is_activated" class="col-md-3 control-label">Activate Account</label>

                                    <div class="col-md-9">
                                        <input name="is_activated" type="radio" value="{{old(0, $user->is_activated)}}"> yes<br>
                                        <input checked="checked" name="is_activated" type="radio" value="{{old(0, $user->is_activated)}}"> no
                                        @if ($errors->has('is_activated'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('is_activated') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="email_de">Account No.</label>
                                    <div class="col-md-9">
                                        <input type="text" name="acct" value="{{ old('acct', $user->account_number) }}"  class="form-control"  id="email_de" maxlength="10" required>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="acct_name">Account  Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="acct_name" value="{{ old('acct_name', $user->acct_name) }}" class="form-control" id="acct_name" required>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-3" for="bank">Bank Name</label>
                                     <div class="col-md-9">
                                         <select class="form-control"  id="bank" name="bank_name">
                                             @foreach($banks as $bank)
                                                 <option value="{{old('bank', $user->bank_name)}}">{{$bank->name}}</option>
                                             @endforeach
                                         </select>
                                     </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-3 control-label">Password</label>

                                    <div class="col-md-9">
                                        <input id="password" type="password" value="{{old('password', $user->password)}}" class="form-control underline-input" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-3 control-label">Confirm Password</label>

                                    <div class="col-md-9">
                                        <input id="password-confirm" type="password"  value="{{old('password', $user->password)}}" class="form-control underline-input" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group bg-slategray lt wrap-reset mt-20 text-left">
                                    <div class="col-md-12 ">
                                        <button type="submit" class="btn btn-success btn-anim btn-block">
                                            Update User
                                        </button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection()
