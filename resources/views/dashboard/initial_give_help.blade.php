@extends('layouts.app')
@section('content')

<?php
$packages =App\Package::all();
$package = App\Package::find(Auth::user()->package_id);


?>

@if(Auth::user()->is_awaiting_sponsor)


    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="panel panel-primary card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">
                        <a href="#">
                            <div class="text-center">
                                <p>PROVIDING YOUR FIRST HELP ON <strong class="label label-primary">{{$package->amount}}</strong> PACKAGE</p>
                                <p class="label label-danger">You have already requested to provide your first help. Wait to be matched</p>

                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @else
<div class="row">
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <div class="panel panel-primary card-view">
            <div class="panel-wrapper collapse in">
                <div class="panel-body contact-card">
                    <a href="#">
                        <div class="text-center">
                            <p class="label label-success">PROVIDE YOUR FIRST HELP</p>
                            <p><em>select a package and continue</em></p>
                        </div>
                    </a>

                    <form name="form" role="form" method="post" action="{{route ('new_order', Auth::user()->id)}}">
                        {{ csrf_field() }}
                        <div class="form-group mt-30 mb-30">
                            <label class="control-label mb-10 text-left">Package</label>
                            <select class="form-control" name="package" required>
                                @foreach($packages as $package)
                                    <option value="{{$package->amount}}">{{$package->amount}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-0">
                            @if(Auth::user()->is_awaiting_sponsor)
                                <button type="submit" class="btn btn-success btn-block btn-anim" disabled><i class="fa fa-user"></i><span class="btn-text">Submit To Get Sponsor</span></button>
                                <br>
                               @else
                                <button type="submit" class="btn btn-success btn-block btn-anim"><i class="fa fa-user"></i><span class="btn-text">Submit To Get Sponsor</span></button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection()