@extends('layouts.app')
@section('content')
    <?php
            $banks = App\Bank::all();
            ?>

    <div class="row">
        <div class="col-sm-5">
            <div class="panel panel-default card-view">
                @include('dashboard.notification')

                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">YOUR PAYMENT INFORMATION</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('message'))
                            <div class="alert-success">{{session('message')}}</div>
                        @endif

                        <div class="form-wrap">
                            <form class="form-group" name="form" method="post" action="{{route('add_payment_info', Auth::user()->id)}}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="control-label mb-10" for="email_de">Account No.</label>
                                    <input type="text" name="acct" value="{{ old('acct', Auth::user()->account_number) }}"  class="form-control"  id="email_de" maxlength="10" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="pwd_de">Account  Name</label>
                                    <input type="text" name="name" value="{{ old('name', Auth::user()->name) }}" class="form-control" id="pwd_de"  placeholder="{{Auth::user()->acct_name}}"required>
                                </div>
                                <div class="form-group mt-30 mb-30">
                                    <label class="control-label mb-10 text-left">Bank Name</label>
                                    <select class="form-control" name="bank">
                                    @foreach($banks as $bank)
                                        <option name="bank" value="{{$bank->name}}">{{$bank->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label mb-10" for="pwd_de">Phone</label>--}}
                                    {{--<input type="text" class="form-control" value="{{ old('phone', Auth::user()->phone) }}" placeholder="{{Auth::user()->phone}}" name="phone" id="pwd_de" maxlength="11" required>--}}
                                {{--</div>--}}
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-success btn-anim btn-block"><i class="fa fa-bank"></i><span class="btn-text">Submit Bank Details</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Bank Data</h6>
                </div>
                <div class="clearfix"></div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body task-panel">

                        <div class="list-group mb-0">
                            <ul>
                                <li href="#" class="list-group-item">
                                    <span class="label label-info pull-right"> {{Auth::user()->acct_name}}</span>
                                    <i class="fa fa-user"></i>Account Name:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{Auth::user()->email}}</span>
                                    <i class="fa fa-inbox"></i> Email:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{Auth::user()->account_number}}</span>
                                    <i class="primary fa fa-credit-card"></i> Account Number:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{Auth::user()->phone}}</span>
                                    <i class="primary fa-phone"></i> Phone:

                                </li>
                                <li href="#" class="list-group-item">
                                    <span class="label label-primary pull-right">{{Auth::user()->bank_name}}</span>
                                    <i class="fa fa-bank"></i> Bank:

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
    </div>
    <!-- /Row -->
@endsection()
