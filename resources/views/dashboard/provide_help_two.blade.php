@extends('layouts.app')
@section('content')

    <?php
    $packages =App\Package::all();
    $package = App\Package::find(Auth::user()->package_id);


    ?>

    <div class="row">
@if ($user->is_awaiting_sponsor == 0 && Auth::user()->sponsor == 0 && Auth::user()->receiver == false)

        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div    class="panel-body contact-card">
                        @if(session('message'))
                            <div class="alert alert-success">{{session('message')}}</div>
                        @endif
                        <p class="alert alert-success">Recycling : Providing another help  to release your second payment</p>

                        <form action="{{route('recycle_order', Auth::user()->id)}}" method="post">
                            {{ csrf_field() }}

                            <a href="#">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-danger btn-anim"><i class="icon-rocket"></i><span class="btn-text">PROVIDE HELP TO RECYCLE</span></button>
                                </div>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">What To Do</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <p  class="txt-dark">Provide Another Help to get your second payment</p>
                                </div>
                            </div>


                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <div class="alert alert-danger">
                                        <p > You Have  an amount of Pending. <span class="label label-success">{{$package->amount}}</span> Recylce to get it back</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif(Auth::user()->sponsor != 0)

    <h4>Sorry you still have a sponsor to pay to</h4>

    @else
       <h4 class="alert alert-info">Click <a class="alert alert-success" href="{{route('dashboard-home')}}">Here</a> for your next instruction</h4>
     @endif

    </div>


@endsection()