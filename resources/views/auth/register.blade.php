@extends('layouts.auth-layout')
@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="panel panel-default card-view mb-0">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title text-center txt-dark">REGISTER</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
<div class="panel-wrapper collapse in">
<div class="panel-body">
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="form-wrap">
            <form class="form-horizontal orm-validation mt-20" name="form" role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <p class="help-block text-left">

                    Enter your personal details below:
                </p>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-3 control-label">Name</label>

                    <div class="col-md-9">
                        <input id="name" type="text" class="form-control underline-input" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-3 control-label">E-Mail</label>

                    <div class="col-md-9">
                        <input id="email" type="email" class="form-control underline-input" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label for="phone" class="col-md-3 control-label">Phone</label>

                    <div class="col-md-9">
                        <input id="phone"  class="form-control underline-input" name="phone" value="{{ old('phone') }}" required>

                        @if ($errors->has('phone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-3 control-label">Password</label>

                    <div class="col-md-9">
                        <input id="password" type="password" class="form-control underline-input" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-3 control-label">Confirm Password</label>

                    <div class="col-md-9">
                        <input id="password-confirm" type="password" class="form-control underline-input" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="checkbox checkbox-custom-alt checkbox-custom-sm inline-block">
                        <input type="checkbox"><i></i> I agree to the <a href="javascript:;">Terms of Service</a> &amp; <a href="javascript:;">Privacy Policy</a>
                    </label>
                    <br>
                    Got an account already?
                    <a href="{{route ('login')}}" class="text-lightred">Login Here</a>

                </div>

                <div class="form-group bg-slategray lt wrap-reset mt-20 text-left">
                    <div class="col-md-12 ">
                        <button type="submit" class="btn btn-danger btn-block">
                            Register
                        </button>
                    </div>
                </div>
                @if(isset($_GET['ref']))
                    <input type="hidden" name="referrer" value="{{ $_GET['ref'] }}">

                @else
                    <input type="hidden" name="referrer" value="1">
                @endif


            </form>

            {{--<form name="form" role="form" method="POST" action="{{ route('register') }}">--}}
                {{--{{ csrf_field() }}--}}
                {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                    {{--<label class="control-label mb-10" for="name">FULL NAME</label>--}}
                    {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" required>--}}
                        {{--<div class="input-group-addon"><i class="icon-user"></i></div>--}}
                    {{--</div>--}}
                    {{--@if ($errors->has('name'))--}}
                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                    {{--<label class="control-label mb-10" for="email">Email address</label>--}}
                    {{--<div class="input-group">--}}
                        {{--<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>--}}
                        {{--<div class="input-group-addon"><i class="icon-envelope-open"></i></div>--}}
                        {{--@if ($errors->has('email'))--}}
                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label class="control-label mb-10" for="password">Password</label>--}}
                    {{--<div class="input-group">--}}
                        {{--<input type="password" class="form-control" name="password" id="password" value="{{old('password')}}" required>--}}
                        {{--<div class="input-group-addon"><i class="icon-lock"></i></div>--}}
                    {{--</div>--}}

                    {{--@if ($errors->has('password'))--}}
                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label class="control-label mb-10" for="password-confirm">Confirm Password</label>--}}
                    {{--<div class="input-group">--}}
                        {{--<input type="password" class="form-control" required="" id="password-confirm" placeholder="">--}}
                        {{--<div class="input-group-addon"><i class="icon-lock"></i></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<div class="checkbox checkbox-success">--}}
                        {{--<input id="checkbox_2" required="" type="checkbox">--}}
                        {{--<label for="checkbox_2"> I agree to all <a class="txt-danger capitalize-font" href="#">terms</a></label>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<button type="submit" class="btn btn-success btn-block">sign up</button>--}}
                {{--</div>--}}
                {{--<div class="form-group mb-0">--}}
                    {{--<span class="inline-block pr-5">Already have an account?</span>--}}
                    {{--<a class="inline-block txt-danger" href="{{ route('login') }}">Sign In</a>--}}
                {{--</div>--}}
            {{--</form>--}}
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
    </div>


@endsection()
