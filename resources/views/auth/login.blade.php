@extends('layouts.auth-layout')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="panel panel-default card-view mb-0">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Sign In ...</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-wrap">
                                            <form  role="form" method="POST" action="{{ url('/login') }}">
                                                {{ csrf_field() }}

                                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                                    <div class="input-group">
                                                        <input id="exampleInputEmail_2" placeholder="Enter email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                        @endif
                                                        <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                    </div>


                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label class="control-label mb-10" for="exampleInputpwd_2">Password</label>
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" name="password" required id="exampleInputpwd_2" placeholder="Enter pwd">
                                                        <div class="input-group-addon"><i class="icon-lock"></i></div>
                                                    </div>
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="form-group">
                                                    <div class="checkbox checkbox-success pr-10 pull-left">
                                                        <input id="checkbox_2" required="" type="checkbox">
                                                        <label for="checkbox_2"> keep me logged in </label>
                                                    </div>
                                                    <a class="capitalize-font txt-danger block pt-5 pull-right" href="{{ url('/password/reset') }}">forgot password</a>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <span class="inline-block pr-5">Don't have an account?</span>
                                                    <a class="inline-block txt-danger" href="{{url('/register')}}">Sign Up</a>
                                                </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

    </div>
    <!-- /Main Content -->
@endsection
