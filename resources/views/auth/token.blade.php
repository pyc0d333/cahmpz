@extends('layouts.auth-layout')

@section('content')

    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float">

                    <div class="panel panel-default card-view mb-0">
                        @include('dashboard.notification')

                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Enter Your Token</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-wrap">
                                            <form  role="form" method="POST" action="{{ route('add_token') }}">
                                                {{ csrf_field() }}

                                                <div class="form-group{{ $errors->has('pin') ? ' has-error' : '' }}">
                                                    <label class="control-label mb-12" for="exampleInputEmail_2">Registration Pin</label>
                                                    <div class="input-group">
                                                        <input id="exampleInputEmail_2" placeholder="Enter Pin" maxlength="6" class="form-control" name="pin" value="{{ old('pin') }}" required autofocus>
                                                        @if ($errors->has('pin'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('pin') }}</strong>
                                    </span>
                                                        @endif
                                                        <div class="input-group-addon"><i class="icon-"></i></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-success btn-block">Add Token</button>
                                                    </div>
                                                    <div class="form-group mb-0">
                                                        <span class="inline-block pr-5">if you don't have a token, text your name and contact to<strong class="label label-primary">08091018824</strong> to request for a Pin</span>
                                                        <span class="inline-block pr-5">You will be instructed on what to do next</span>
                                                     </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

    </div>
    <!-- /Main Content -->
@endsection
