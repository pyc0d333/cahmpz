@extends('layouts.auth-layout')
@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="panel panel-default card-view mb-0">
                        <div class="panel-heading">
@if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Welcome <strong>{{Auth::user()->name}}</strong></h6>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <a href="{{route('dashboard-home')}}" class="btn btn-success btn-large">Continue</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

    </div>
    <!-- /Main Content -->
@endsection
